cfz-www README
==================

Getting Started
---------------

- cd <directory containing this file>

- $venv/bin/python setup.py develop

- $venv/bin/initialize_cfz-www_db development.ini

- $venv/bin/pserve development.ini

