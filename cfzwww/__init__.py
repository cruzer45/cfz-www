from pyramid.config import Configurator
from pyramid_beaker import session_factory_from_settings
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from sqlalchemy import engine_from_config

from webassets import Bundle

from .models import (
    DBSession,
    Base)
from .services import auth

from cfzwww.scripts.initializedb import (
    get_url,
    map_models)

from cfzcore.auth import groupfinder


def group_finder(user_id, request):
    user_account = auth.find_user_by_id(user_id)
    return groupfinder(user_account)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    :param global_config:
    :param settings:
    """
    map_models(get_url(global_config['__file__']))
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    session_factory = session_factory_from_settings(settings)
    authn_policy = AuthTktAuthenticationPolicy('s0secret')
    authn_policy.timeout = 1200
    authn_policy.reissue_time = 120
    authn_policy.callback = group_finder
    authz_policy = ACLAuthorizationPolicy()
    config = Configurator(settings=settings,
                          authentication_policy=authn_policy,
                          authorization_policy=authz_policy,
                          session_factory=session_factory,
                          root_factory='cfzwww.models.RootFactory')

    config.include('pyramid_jinja2')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.include('pyramid_webassets')
    config.add_jinja2_extension('webassets.ext.jinja2.AssetsExtension')
    assets_env = config.get_webassets_env()
    jinja2_env = config.get_jinja2_environment()
    jinja2_env.assets_environment = assets_env

    ############ webassets #############
    trial_coffee = Bundle('coffee/trial.coffee',
                          filters='coffeescript',
                          output='trial.js', debug=False)
    config.add_webasset('trial_coffee', trial_coffee)

    handlebars = Bundle('javascript/libs/handlebars.runtime.js', filters='rjsmin')
    jQuery = Bundle('javascript/libs/jquery.js', filters='rjsmin')
    jQueryUi = Bundle(
        Bundle('javascript/libs/jquery-ui/js/jquery-ui-1.10.0.custom.js',
               filters='rjsmin',
               output='jqueryUi'),
        Bundle('javascript/libs/jquery-ui/css/smoothness/jquery-ui-1.10.0.custom.css')
    )
    chosen_jquery = Bundle('kuta/plugins/chosen.jquery.min.js',
                           filters='rjsmin')
    underscore = Bundle('javascript/libs/underscore.js',
                        filters='rjsmin')
    backbone = Bundle('javascript/libs/backbone.js',
                      filters='rjsmin')
    marionette = Bundle('javascript/libs/marionettejs/*js',
                        filters='rjsmin')
    backbone_paginator = Bundle('javascript/libs/backbone.paginator.js',
                                filters='rjsmin')
    bootstrap_plugins = Bundle('kuta/plugins/bootstrap-plugins.js',
                               filters='rjsmin')
    editable_plugin = Bundle('javascript/libs/editable/bootstrap-editable.js',
                             filters='rjsmin',
                             output='bootstrap-editable')
    momentjs = Bundle('javascript/libs/moment.js', filters='rjsmin')
    spinjs = Bundle('javascript/libs/spin.js', filters='rjsmin')

    company_list_templates = Bundle(
        Bundle('coffee/company/views/templates/results_count.handlebars.js'),
        Bundle('coffee/company/views/templates/company_list_header.handlebars.js'),
        Bundle('coffee/company/views/templates/company_list_body.handlebars.js'),
        filters='rjsmin')

    company_show_templates = Bundle(
        Bundle('coffee/company/views/templates/company_nav.handlebars.js'),
        Bundle('coffee/company/views/templates/acceptance_date.handlebars.js'),
        Bundle('coffee/company/views/templates/acceptance_date_form.handlebars.js'),
        Bundle('coffee/company/views/templates/company_general_information.handlebars.js'),
        Bundle('coffee/company/views/templates/contact_numbers.handlebars.js'),
        Bundle('coffee/company/views/templates/contact_numbers_form.handlebars.js'),
        Bundle('coffee/company/views/templates/address_empty.handlebars.js'),
        Bundle('coffee/company/views/templates/address_table.handlebars.js'),
        Bundle('coffee/company/views/templates/directors_list.handlebars.js'),
        Bundle('coffee/company/views/templates/directors_list_header.handlebars.js'),
        Bundle('coffee/company/views/templates/director_form.handlebars.js'),
        Bundle('coffee/company/views/templates/shareholders_header.handlebars.js'),
        Bundle('coffee/company/views/templates/shareholders_list.handlebars.js'),
        Bundle('coffee/company/views/templates/shareholders_form.handlebars.js'),
        Bundle('coffee/company/views/templates/annualreport_header.handlebars.js'),
        Bundle('coffee/company/views/templates/annualreport_list.handlebars.js'),
        Bundle('coffee/company/views/templates/annualreport_form.handlebars.js'),
        Bundle('coffee/company/views/templates/vehiclepass_header.handlebars.js'),
        Bundle('coffee/company/views/templates/vehiclepass_list.handlebars.js'),
        Bundle('coffee/company/views/templates/vehiclepass_form.handlebars.js'),
        Bundle('coffee/company/views/templates/vehiclepass_layout.handlebars.js'),
        Bundle('coffee/company/views/templates/vehiclepass_create_btn.handlebars.js'),
        Bundle('coffee/company/views/templates/vehiclepass_maxnumber_form.handlebars.js'),
        Bundle('coffee/company/views/templates/outlet_header.handlebars.js'),
        Bundle('coffee/company/views/templates/outlet_list.handlebars.js'),
        Bundle('coffee/company/views/templates/outlet_form.handlebars.js'),
        Bundle('coffee/company/views/templates/outlet_layout.handlebars.js'),
        Bundle('coffee/company/views/templates/outlet_create_button.handlebars.js'),
        Bundle('coffee/company/views/templates/goods_categories_layout.handlebars.js'),
        Bundle('coffee/company/views/templates/company_goods_category.handlebars.js'),
        Bundle('coffee/company/views/templates/company_no_goods_category.handlebars.js'),
        Bundle('coffee/company/views/templates/goods_options_view.handlebars.js'),
        Bundle('coffee/company/views/templates/courtesy_pass_layout.handlebars.js'),
        Bundle('coffee/company/views/templates/courtesypass_header.handlebars.js'),
        Bundle('coffee/company/views/templates/courtesypass_list.handlebars.js'),
        Bundle('coffee/company/views/templates/courtesypass_create_btn.handlebars.js'),
        Bundle('coffee/company/views/templates/courtesypass_empty.handlebars.js'),
        Bundle('coffee/company/views/templates/courtesypass_form.handlebars.js'),
        Bundle('coffee/company/views/templates/authorizedpersons_layout.handlebars.js'),
        Bundle('coffee/company/views/templates/authorizedpersons_header.handlebars.js'),
        Bundle('coffee/company/views/templates/authorizedpersons_list.handlebars.js'),
        Bundle('coffee/company/views/templates/authorizedpersons_empty.handlebars.js'),
        Bundle('coffee/company/views/templates/authorizedpersons_createbtn.handlebars.js'),
        Bundle('coffee/company/views/templates/authorizedpersons_form.handlebars.js'),
        Bundle('coffee/company/views/templates/opcontracts_layout.handlebars.js'),
        Bundle('coffee/company/views/templates/opcontracts_list.handlebars.js'),
        Bundle('coffee/company/views/templates/opcontracts_empty.handlebars.js'),
        Bundle('coffee/company/views/templates/opcontracts_createbtn.handlebars.js'),
        Bundle('coffee/company/views/templates/opcontracts_header.handlebars.js'),
        filters='rjsmin',
        output='js/app/company-templates.js')

    company_list_app = Bundle(
        Bundle('coffee/config/renderer.coffee'),
        Bundle('coffee/company/CompanyListApp.coffee'),
        Bundle('coffee/company/models/CompanyModel.coffee'),
        Bundle('coffee/company/models/ResultsMeta.coffee'),
        Bundle('coffee/company/views/CompanyListView.coffee'),
        Bundle('coffee/company/views/CompanyItemView.coffee'),
        Bundle('coffee/company/collections/CompanyPaginatedCollection.coffee'),
        Bundle('coffee/company/collections/Companies.coffee'),
        Bundle('coffee/company/CompanyListRouter.coffee'),
        filters='coffeescript',
        output='js/app/company-list.js', debug=False)

    company_builder = Bundle(
        Bundle('coffee/company/CompanyBuilderApp.coffee'),
        Bundle('coffee/company/models/CompanyFormModel.coffee'),
        Bundle('coffee/company/views/CompanyBuildFormView.coffee'),
        filters='coffeescript',
        output='js/app/company-builder.js', debug=False)

    outlet_builder = Bundle(
        Bundle('coffee/outlet/OutletCreateApp.coffee'),
        Bundle('coffee/outlet/models/OutletFormModel.coffee'),
        Bundle('coffee/outlet/views/OutletCreateFormView.coffee'),
        filters='coffeescript',
        output='js/app/outlet-builder.js', debug=False)

    _company_app = Bundle(
        Bundle('coffee/apps/company/app.coffee'),
        Bundle('coffee/apps/company/navbar/navbar_app.coffee'),
        Bundle('coffee/apps/company/navbar/navbar_views.coffee'),
        Bundle('coffee/apps/company/contactNumbers/app.coffee'),
        Bundle('coffee/apps/company/contactNumbers/views.coffee'),
        Bundle('coffee/apps/company/address/address_app.coffee'),
        Bundle('coffee/apps/company/address/address_views.coffee'),
        Bundle('coffee/apps/company/entities/company.coffee'),
        Bundle('coffee/apps/company/entities/contacts.coffee'),
        Bundle('coffee/apps/company/entities/address.coffee'),
        Bundle('coffee/apps/company/entities/director.coffee'),
        Bundle('coffee/apps/company/entities/shareholder.coffee'),
        Bundle('coffee/apps/company/entities/annual_report.coffee'),
        Bundle('coffee/apps/company/entities/outlet.coffee'),
        Bundle('coffee/apps/company/entities/vehicle_sticker.coffee'),
        Bundle('coffee/apps/company/entities/operational_contracts.coffee'),
        Bundle('coffee/apps/company/opcontracts/opcontractViews.coffee'),
        Bundle('coffee/apps/company/opcontracts/opcontractApp.coffee'),
        Bundle('coffee/apps/company/generalInformation/general_information_app.coffee'),
        Bundle('coffee/apps/company/generalInformation/general_information_views.coffee'),
        Bundle('coffee/apps/company/director/director_app.coffee'),
        Bundle('coffee/apps/company/director/director_show_view.coffee'),
        Bundle('coffee/apps/company/director/director_list_view.coffee'),
        Bundle('coffee/apps/company/shareholder/shareholder_app.coffee'),
        Bundle('coffee/apps/company/shareholder/shareholder_views.coffee'),
        Bundle('coffee/apps/company/annualReports/reports_views.coffee'),
        Bundle('coffee/apps/company/annualReports/reports_app.coffee'),
        Bundle('coffee/apps/company/vehicleStickers/vehicle_stickers_views.coffee'),
        Bundle('coffee/apps/company/vehicleStickers/vehicle_stickers_app.coffee'),
        Bundle('coffee/apps/company/outlet/outlet_views.coffee'),
        Bundle('coffee/apps/company/outlet/outlet_app.coffee'),
        Bundle('coffee/apps/company/goodsCategories/goods_categories_views.coffee'),
        Bundle('coffee/apps/company/goodsCategories/goods_categories_controller.coffee'),
        Bundle('coffee/apps/company/entities/goods_category.coffee'),
        Bundle('coffee/apps/company/spinner_controller.coffee'),
        Bundle('coffee/apps/company/courtesyPass/courtesyPassController.coffee'),
        Bundle('coffee/apps/company/courtesyPass/courtesyPassViews.coffee'),
        Bundle('coffee/apps/company/entities/courtesy_pass.coffee'),
        Bundle('coffee/apps/company/authorizedPersons/authorizedPersonsViews.coffee'),
        Bundle('coffee/apps/company/entities/authorizedpersons.coffee'),
        Bundle('coffee/apps/company/companyTypes/entities.coffee'),
        filters='coffeescript',
        output='js/app/_company-app.js', debug=False)

    outlet_app = Bundle(
        Bundle('coffee/apps/outlet/app.coffee'),
        Bundle('coffee/apps/outlet/general_information_views.coffee'),
        Bundle('coffee/apps/outlet/nav_bar_view.coffee'),
        Bundle('coffee/apps/outlet/contact_numbers/views.coffee'),
        Bundle('coffee/apps/outlet/contact_numbers/controller.coffee'),
        Bundle('coffee/apps/outlet/address/address_views.coffee'),
        Bundle('coffee/apps/outlet/address/address_controller.coffee'),
        Bundle('coffee/apps/outlet/general_information_controller.coffee'),
        Bundle('coffee/apps/outlet/manager/manager_views.coffee'),
        Bundle('coffee/apps/outlet/manager/manager_controller.coffee'),
        Bundle('coffee/apps/outlet/entities/outlet.coffee'),
        Bundle('coffee/apps/outlet/entities/contact_numbers.coffee'),
        Bundle('coffee/apps/outlet/entities/address.coffee'),
        Bundle('coffee/apps/outlet/entities/manager.coffee'),
        filters='coffeescript',
        output='js/app/outlet-app.js', debug=False)

    outlet_templates = Bundle(
        Bundle('coffee/apps/outlet/contact_numbers/templates/contact_table.handlebars.js'),
        Bundle('coffee/apps/outlet/contact_numbers/templates/contact_form.handlebars.js'),
        Bundle('coffee/apps/outlet/address/templates/address_form.handlebars.js'),
        Bundle('coffee/apps/outlet/address/templates/address_table.handlebars.js'),
        Bundle('coffee/apps/outlet/manager/templates/manager_table.handlebars.js'),
        Bundle('coffee/apps/outlet/manager/templates/manager_form.handlebars.js'),
        filters='rjsmin',
        output='js/app/outlet-templates.js')

    goods_categories = Bundle(
        Bundle('coffee/apps/goods_categories/application.coffee'),
        Bundle('coffee/apps/goods_categories/views.coffee'),
        Bundle('coffee/apps/goods_categories/entities.coffee'),
        filters='coffeescript',
        output='js/app/goods-categories-app.js', debug=False)

    goods_categories_templates = Bundle(
        Bundle('coffee/apps/goods_categories/templates/table_header.handlebars.js'),
        Bundle('coffee/apps/goods_categories/templates/table_body.handlebars.js'),
        Bundle('coffee/apps/goods_categories/templates/goods_category_empty.handlebars.js'),
        Bundle('coffee/apps/goods_categories/templates/goods_category_form.handlebars.js'),
        filters='rjsmin')

    config.add_webasset('jQuery', jQuery)
    config.add_webasset('jQueryUi', jQueryUi)
    config.add_webasset('handlebars', handlebars)
    config.add_webasset('underscore', underscore)
    config.add_webasset('backbone', backbone)
    config.add_webasset('marionette', marionette)
    config.add_webasset('backbone_paginator', backbone_paginator)
    config.add_webasset('company_list_app', company_list_app)
    config.add_webasset('company_builder', company_builder)
    config.add_webasset('bootstrap_plugins', bootstrap_plugins)
    config.add_webasset('momentjs', momentjs)
    config.add_webasset('outlet_builder', outlet_builder)
    config.add_webasset('_company_app', _company_app)
    config.add_webasset('outlet_app', outlet_app)
    config.add_webasset('outlet_templates', outlet_templates)
    config.add_webasset('company_list_templates', company_list_templates)
    config.add_webasset('company_show_templates', company_show_templates)
    config.add_webasset('chosen_jquery', chosen_jquery)
    config.add_webasset('spinjs', spinjs)
    config.add_webasset('editable_plugin', editable_plugin)
    config.add_webasset('goods_categories_templates', goods_categories_templates)
    config.add_webasset('goods_categories', goods_categories)

    config.add_route('test', '/test')

    config.add_route('index', '/')

    config.add_route('login', '/login')
    config.add_route('doLogin', '/doLogin')
    config.add_route('logout', '/logout')

    config.add_route('applications', '/applications')
    config.add_route('new_application_form', '/applications/form')
    config.add_route('create_application', '/applications/create')
    config.add_route('show_application', '/applications/show/{id}')
    config.add_route('pending_applications',
                     '/applications/pending')
    config.add_route('pending_applications_list',
                     '/applications/pending/list')
    config.add_route('approve_application',
                     '/applications/approve/{id}')
    config.add_route('denied_applications_list',
                     '/applications/deny/list')
    config.add_route('deny_application',
                     '/applications/deny/{id}')
    config.add_route('get_application', '/applications/{id}')
    config.add_route('update_general_information',
                     '/applications/{id}/general_information')
    config.add_route('update_general_information_ok',
                     '/applications/{id}/general_information/ok')
    config.add_route('update_application_contact_information',
                     '/applications/{id}/contact_information')
    config.add_route('update_application_address',
                     '/applications/{id}/address')
    config.add_route('save_application_consultant',
                     '/applications/{id}/consultant')
    config.add_route('save_application_manager',
                     '/applications/{id}/manager')
    config.add_route('save_application_description_of_goods',
                     '/applications/{id}/description_of_goods')

    # Routes for Application Investors
    config.add_route('application_investors',
                     '/applications/{id}/investors')
    config.add_route('application_add_investor',
                     '/applications/{id}/investors/create')
    config.add_route('application_remove_investor',
                     '/applications/{application_id}/investors/delete/{id}')

    config.add_route('investor_pass_background_check',
                     '/investors/{id}/background_check/pass')
    config.add_route('investor_fail_background_check',
                     '/investors/{id}/background_check/fail')

    #Routes For Company
    config.add_route('show_company', '/company/show/{id}')
    config.add_route('company_form', '/company/form')
    config.add_route('company', '/company/{id}')
    config.add_route('company_index',
                     '/companies')
    config.add_route('company_list',
                     '/companies/list')
    config.add_route('search_for_company_with_name',
                     '/companies/search/{company_name}')
    config.add_route('save_company_contact_numbers',
                     '/company/{id}/contact_numbers')
    config.add_route('company_address', '/company/{id}/address')
    config.add_route('vehiclepass_maxnumbers', '/company/{id}/max_vehiclepass')

    # Routes for shareholders
    config.add_route('company_shareholders',
                     "/company/{id}/shareholders")
    config.add_route('save_company_shareholder',
                     "/company/{id}/shareholder")

    # Company Directors
    config.add_route('save_company_director',
                     '/company/{id}/director')
    config.add_route('company_directors',
                      '/company/{id}/directors')

    # Company Annual Reports
    config.add_route('save_company_annual_report',
                     '/company/{id}/annual_report')
    config.add_route("company_annual_reports",
                     '/company/{id}/annual_reports')

    config.add_route('save_company_date_delivered_letter_of_acceptance',
                     '/company/{id}/letter_of_acceptance')
    config.add_route('company_selectbox', '/company/selectbox')

    #Route for Vehicle Pass
    config.add_route("vehicle_pass_list", "/vehicle_pass/list")
    config.add_route('vehicle_pass_index', '/vehicle_pass/index')
    config.add_route('vehicle_pass_create', '/vehicle_pass')
    config.add_route('vehicle_pass_search_by_owner', '/vehicle_pass/search/{owner_name}')
    config.add_route('max_passes', '/company/{id}/max_vehiclepass')

    # Routes for stickers
    config.add_route('stickers', "/company/{id}/stickers")
    config.add_route('sticker', "/company/{id}/sticker")
    config.add_route('max_stickers', "/company/{id}/max_stickers/{max}")

    # Company goods categories
    config.add_route('company_goods', '/company/{id}/goods')
    config.add_route('company_good', '/company/{id}/good/{category_id}')

    # Courtesy pass
    config.add_route('courtesypass_new', '/company/{company_id}/courtesypass')
    config.add_route('courtesypass', '/company/{company_id}/courtesypass/{courtesypass_id}')
    config.add_route('courtesypass_list', '/company/{company_id}/courtesypass_list')

    # Authorized Pesons
    config.add_route('representation_type', '/representation_types')
    config.add_route('authorizedrepresentative_new', '/company/{company_id}/authorizedrepresentative')
    config.add_route('authorizedrepresentative', '/company/{company_id}/authorizedrepresentative/{id}')
    config.add_route('authorizedrepresentatives_list', '/company/{company_id}/authorizedrepresentatives_list')
    config.add_route('authorizedrepresentatives_delete', '/company/{company_id}/authorizedrepresentatives_list/{id}')

    # Routes for outlets
    config.add_route('create_outlet', "/company/{id}/outlet/create")
    config.add_route('company_outlets', "/company/{id}/outlets")
    config.add_route('outlet_show', "/outlet/show/{id}")
    config.add_route('outlet_api', "/outlet/{id}")
    config.add_route('outlet_api_create', "/company/{id}/outlet")
    config.add_route('outlet_contact_numbers', "/outlet/{id}/contact_numbers")
    config.add_route('outlet_address', "/outlet/{id}/address")

    #Outlet Manager
    config.add_route('outlet_manager', "/outlet/{id}/manager")

    #Outlet Stickers
    config.add_route('outlet_stickers', "/outlet/{id}/stickers")
    config.add_route('outlet_sticker', "/outlet/{id}/sticker")
    config.add_route('outlet_collection_sticker', "/outlet/{outlet_id}/sticker/{sticker_id}")

    #Goods Categories
    config.add_route('goods_categories', '/goods')
    config.add_route('goods_category', '/good_category')
    config.add_route('goods_categories_show', '/goods_categories/show')
    config.add_route('goods_category_item', '/good_category/{id}')

    #Company Types
    config.add_route('company_types', '/company_types')

    #Operational Contracts
    config.add_route('operational_contract', '/company/{id}/operational_contract')
    config.add_route('operational_contract_list', '/company/{id}/operational_contracts')
    config.add_route('operational_contract_print', '/operational_contract/{id}')

    #User Admin
    config.add_route('user_new', '/user/new')
    config.add_route('user_create', '/user/create')
    config.add_route('user_show', '/user/show/{id}')
    config.add_route('user_list', '/user/list')
    config.add_route('user_update', '/user/update/{id}')

    #Access Letters
    config.add_route('access_letter_main', '/access_letters')
    config.add_route('access_letter_new', '/access_letter/{letter_type}/new')
    config.add_route('access_letter_create', '/access_letter/{letter_type}/create')
    config.add_route('access_letter_list', '/access_letter/list/{letter_type}')
    config.add_route('access_letter_show', '/access_letter/{id}')

    config.scan()
    return config.make_wsgi_app()
