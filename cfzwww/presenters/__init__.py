from datetime import datetime


class ApplicationPresenter(object):

    __slots__ = {"id", "company", "manager", "location", "description_of_goods",
                 "status", "consultant"}

    def __init__(self, application):
        company = application.company
        self.company = {'id': company.id,
                        'name': company.name,
                        'company_type': company.company_type,
                        'registration_number': company.registration_number,
                        'cable_number': company.cable_number,
                        'fax_number': company.fax_number,
                        'telephone_number': company.telephone_number}
        manager = company.manager
        self.manager = {'id': manager.id,
                        'first_name': manager.first_name,
                        'last_name': manager.last_name,
                        'full_name': manager.first_name + " " + manager.last_name}
        self.location = {'country': application.country,
                         'state': application.state}
        self.description_of_goods = application.description_of_goods
        self.status = application.status
        self.id = application.id
        consultant = application.consultant
        if application.consultant:
            self.consultant = {'id': consultant.id, 'first_name': consultant.first_name, 'last_name': consultant.last_name}

    def __json__(self, request):
        return {'id': self.id,
                'company': self.company,
                'manager': self.manager,
                'location': self.location,
                'description_of_goods': self.description_of_goods,
                'status': self.status,
                'consultant': getattr(self, "consultant", None)
                }


class DeniedApplicationPresenter(object):

    __slots__ = {"id", "company", "manager", "location", "description_of_goods",
                 "status", "consultant", "denied_date"}

    def __init__(self, application):
        company = application.company
        self.company = {'id': company.id,
                        'name': company.name,
                        'company_type': company.company_type,
                        'registration_number': company.registration_number,
                        'cable_number': company.cable_number,
                        'fax_number': company.fax_number,
                        'telephone_number': company.telephone_number}
        manager = company.manager
        self.manager = {'id': manager.id,
                        'first_name': manager.first_name,
                        'last_name': manager.last_name,
                        'full_name': manager.name()}
        self.location = {'country': application.country,
                         'state': application.state}
        self.description_of_goods = application.description_of_goods
        self.status = application.status
        self.id = application.id
        self.denied_date = datetime.strftime(application.denied_date, "%d/%b/%Y")
        consultant = application.consultant
        if application.consultant:
            self.consultant = {'id': consultant.id, 'first_name': consultant.first_name, 'last_name': consultant.last_name}

    def __json__(self, request):
        return {'id': self.id,
                'company': self.company,
                'manager': self.manager,
                'location': self.location,
                'description_of_goods': self.description_of_goods,
                'status': self.status,
                'denied_date': self.denied_date,
                'consultant': getattr(self, "consultant", None)
                }


class PersonPresenter(object):
    __slots__ = {"id", "first_name", "last_name"}

    def __init__(self, person):
        self.id = person.id
        self.first_name = person.first_name
        self.last_name = person.last_name

    def __json__(self, request):
        return {'id': self.id,
                'first_name': self.first_name,
                'last_name': self.last_name}


class InvestorPresenter(object):
    __slots__ = {"id",
                 "first_name",
                 "last_name",
                 "percent_owned",
                 "background_check"}

    def __init__(self, investor):
        self.id = investor.id
        self.first_name = investor.person.first_name
        self.last_name = investor.person.last_name
        self.percent_owned = investor.percent_owned
        self.background_check = investor.background_check

    def __json__(self, request):
        return {'id': self.id,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'percent_owned': self.percent_owned,
                'background_check': self.background_check}


class ShareholderPresenter(object):
    __slots__ = ("id",
                 "first_name",
                 "last_name",
                 "shares",
                 "background_check")

    def __init__(self, investor):
        self.id = investor.id
        self.first_name = investor.person.first_name
        self.last_name = investor.person.last_name
        self.shares = investor.shares
        self.background_check = investor.background_check

    def __json__(self, request):
        return {"id": self.id,
                "first_name": self.first_name,
                "last_name": self.last_name,
                "shares": self.shares,
                "background_check": self.background_check}
