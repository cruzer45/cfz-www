class RepresentationTypePresenter(object):
    __slots__ = ['id', 'name']

    def __init__(self, representation_type):
        self.id = representation_type.id
        self.name = representation_type.name

    def __json__(self, request):
        return {
                'id': self.id,
                'name': self.name
                }


class AuthorizedRepresentativePresenter(object):
    __slots__ = ['id',
                 'first_name',
                 'last_name',
                 'name',
                 'company_id',
                 'representation_type',
                 'signature']

    def __init__(self, authorizedrepresentative):
        self.id = authorizedrepresentative.id
        self.first_name = authorizedrepresentative.first_name
        self.last_name = authorizedrepresentative.last_name
        self.name = authorizedrepresentative.name
        self.company_id = authorizedrepresentative.company.id
        self.representation_type = { 'id': authorizedrepresentative.representation_type.id,
                                     'name': authorizedrepresentative.representation_type.name}
        self.signature = authorizedrepresentative.signature

    def __json__(self, request):
        return {
                'id': self.id,
                'first_name': self.first_name,
                'last_name': self.last_name,
                'name': self.name,
                'company_id': self.company_id,
                'representation_type': self.representation_type,
                'signature': self.signature
               }
