class OperationContractPresenter(object):
    __slots__ = ['id', 
                 'cfz_ceo', 
                 'cfz_office',
                 'director',
                 'company',
                 'seal_date',
                 'processing_fee',
                 'company_types',
                 'goods',
                 'pretty_seal_date']

    def __init__(self, opcontract):
        self.id = opcontract.id
        self.cfz_ceo = opcontract.cfz_ceo
        self.cfz_office = opcontract.cfz_office
        self.director = opcontract.director
        self.seal_date = opcontract.seal_date.strftime('%d/%m/%Y')
        self.pretty_seal_date = opcontract.seal_date.strftime('%d/%b/%Y')
        self.processing_fee = str(opcontract.processing_fee)
        self.company = opcontract.company.name
        self.company_types = opcontract.company_types
        self.goods = opcontract.goods

    def __json__(self, request):
        return{
                'id': self.id,
                'cfz_ceo': self.cfz_ceo,
                'cfz_office': self.cfz_office,
                'director': self.director,
                'seal_date': self.seal_date,
                'pretty_seal_date': self.pretty_seal_date,
                'processing_fee': self.processing_fee,
                'company_types': self.company_types,
                'goods': self.goods,
                'company': self.company
                }
