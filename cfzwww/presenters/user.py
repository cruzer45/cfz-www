class UserPresenter(object):
    def __init__(self, user):
        self.id = user.id
        self.username = user.username
        self.email = user.email
        self.groups = [group.group_name for group in user.groups]

    def __json__(self, request):
        return {
                'id': self.id,
                'username': self.username,
                'email': self.email,
                'groups': self.groups
                }

