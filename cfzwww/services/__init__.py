from .company_data_service import CompanyService
from .authorizedrepresentative import (AuthorizedRepresentativeCrudService,
                                       RepresentationTypeCrudService)
from ..models import (company_repository,
                      representation_repository,
                      authorizedrep_repository,
                     )


company_crudservice = CompanyService(company_repository)
representationtype_crudservice = RepresentationTypeCrudService(representation_repository)
authorizedrep_crudservice = AuthorizedRepresentativeCrudService(authorizedrep_repository)


