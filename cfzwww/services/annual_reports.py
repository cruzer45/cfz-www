from cfzwww.models import DBSession
from cfzpersistence.repository.company import CompanyRepository
from cfzcore.company import CompanyAnnualReport
from cfzcore.interactors.company import create_annual_report_for_company


class AnnualReportCrudService(object):
    def __init__(self, repository=CompanyRepository(DBSession)):
        self.repository = repository

    def create_annual_report(self, company_id, **kwargs):
        company = self.repository.get(company_id)
        annual_report = create_annual_report_for_company(company, **kwargs)
        self.repository.persist(company)
        return annual_report
