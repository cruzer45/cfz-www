from cfzpersistence.repository.auth import UserAccountRepository
from ..models import DBSession

def find_user_by_id(id):
	repository = UserAccountRepository(DBSession)
	return repository.get(id)

