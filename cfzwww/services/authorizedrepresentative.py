from ..models import (authorizedrep_repository,
                      company_repository,
                      representation_repository,
                     )

from cfzcore.company import AuthorizedRepresentative
from datetime import datetime


class AuthorizedRepresentativeCrudService(object):
    def __init__(self, repository=authorizedrep_repository):
        self.repository = repository

    def create_authorizedrepresentative(self, **kwargs):
        authorizedrepresentative = AuthorizedRepresentative.create(**kwargs)
        return self.repository.persist(authorizedrepresentative)

    def list_authorizedrepresentatives(self, company):
        return self.repository.all_for_company(company)

    def get(self, id):
        '''Retrieve an authorized representative with the matching id.'''
        return self.repository.get(int(id))

    def delete(self, representative_id):
        '''Delete a representative from the databse.

        This doesn't really delete it, just makes it invisible. It also
        sets the date_deleted field to the current date.
        '''
        representative = self.get(int(representative_id))
        return self.repository.delete(representative)


class RepresentationTypeCrudService(object):
    def __init__(self, repository=representation_repository):
        self.repository = repository

    def get(self, id):
        return self.repository.get(int(id))

    def all(self):
        return self.repository.all()
