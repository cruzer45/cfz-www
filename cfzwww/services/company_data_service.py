from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository
from cfzcore.interactors.company import CompanyCountry, CompanyContactNumbers
from ..models import (
    company_type_repository,
    company_repository,
    director_repository)
    #DBSession)
from datetime import datetime
import logging


class CompanyService(object):
    def __init__(self, company_repository=company_repository):
        self.company_repository = company_repository
        self.log = logging.getLogger(__name__)

    def get_with_id(self, company_id):
        company = self.company_repository.get(company_id)
        return company

    def convert_shareholders_to_list(self, shareholders):
        _shareholders = []
        for shareholder in shareholders:
            _shareholder = dict(id=shareholder.id,
                                first_name=shareholder.person.first_name,
                                last_name=shareholder.person.last_name,
                                shares=shareholder.shares,
                                background_check=shareholder.background_check)
            _shareholders.append(_shareholder)
        return _shareholders

    def update_country(self, company_id, **kwargs):
        company_address = CompanyCountry(
            int(company_id),
            self.company_repository)
        return company_address.update(**kwargs)

    def create_address(self, **kwargs):
        from cfzcore.address import Address
        company = self.company_repository.get(kwargs.get('company_id'))
        address = Address()
        address.line1 = kwargs.get('plaza')
        address.line2 = kwargs.get('locale')
        company.address = address
        company = self.company_repository.persist(company)
        return company

    def update_address(self, **kwargs):
        company = self.company_repository.get(kwargs.get('company_id'))
        company.address.line1 = kwargs.get('plaza')
        company.address.line2 = kwargs.get('locale')
        company = self.company_repository.persist(company)
        return company

    def update_contact_numbers(self, company_id, **kwargs):
        company_numbers = CompanyContactNumbers(
            company_id,
            self.company_repository)
        return company_numbers.update(**kwargs)

    def update(self, company_id, **kwargs):
        #self.update_address(**kwargs)
        #self.update_contact_numbers(company_id, **kwargs)
        #self.update_director(company_id, **kwargs)
        company = self.company_repository.get(company_id)
        company.status = kwargs.get('status', None)
        return company

    def update_max_vehiclepass(self, company_id, max_vehiclepass):
        company = self.get_with_id(company_id)
        company.max_vehicles = max_vehiclepass
        company = self.company_repository.persist(company)
        return company

    def save_date_letter_of_acceptance_was_delivered(self, company_id, date):
        company = self.company_repository.get(company_id)
        date_letter_sent = datetime.strptime(date, "%d/%m/%Y")
        company.date_sent_letter_of_acceptance = date_letter_sent
        company = self.company_repository.persist(company)
        return company

    def get_directors_for_company_with(self, company_id):
        '''Return a list of directors for a company with the specified id.
        '''
        from ..presenters.company import DirectorViewModel
        from cfzpersistence.repository import NullRecordException
        from operator import attrgetter
        try:
            if company_id:
                company = self.company_repository.get(company_id)
                directors = [DirectorViewModel(director) for director in company.directors]
                return sorted(directors, key=attrgetter('start_date'), reverse=True)
            else:
                return DirectorViewModel()
        except NullRecordException, exception:
            self.log.debug("Company does not exist with the specified id: ", company_id)
            return DirectorViewModel()

    def save_director(self, company_id, **kwargs):
        from cfzcore.interactors.company import CompanyDirectorEditor
        from cfzcore.person import Person
        from cfzcore.company import Director
        from datetime import datetime
        director_editor = CompanyDirectorEditor()
        company = self.company_repository.get(company_id)
        if kwargs.get('id'):
            director = director_repository.get(kwargs.get('id'))
            director = director_editor.modify(director, **kwargs)
            director = director_repository.persist(director)
            director.end_date = datetime.strptime(kwargs.get('end_date'), "%d/%m/%Y") if kwargs.get('end_date') else None
            return director
        else:
            person = Person(kwargs.get('first_name'), kwargs.get('last_name'))
            person.email = kwargs.get('email', None)
            person.phone1 = kwargs.get('phone1', None)
            person.phone2 = kwargs.get('phone2', None)
            director = Director(person, datetime.strptime(kwargs.get('start_date'), "%d/%m/%Y"))
            director.end_date = datetime.strptime(kwargs.get('end_date'), "%d/%m/%Y") if kwargs.get('end_date') else None
            return director_editor.add_to_company(company, director)

class CompanyBuildService(object):
    def __init__(self, company_repository=company_repository):
        self.repository = company_repository

    def create(self, **kwargs):
        from cfzcore.company import (CompanyParams, Company, Director)
        from cfzcore.interactors.company import CompanyDirectorEditor
        from cfzcore.person import Person
        params = CompanyParams(name=kwargs.get('name'),
                               registration_number=kwargs.get('registration_number'))
        company = Company(params)
        company.status = kwargs.get('status')
        company.company_types.append(company_type_repository.find_by_name(kwargs.get('company_type')))
        company.active = True
        company.incorporation_date = datetime.strptime(kwargs.get('incorporation_date', None), "%d/%m/%Y")
        company = self.repository.persist(company)
        director = Director(Person(kwargs.get('first_name'), kwargs.get('last_name')),
                          datetime.strptime(kwargs.get('start_date'), "%d/%m/%Y"))
        directorEditor = CompanyDirectorEditor()
        directorEditor.add_to_company(company, director)
        company = self.repository.persist(company)
        return company
