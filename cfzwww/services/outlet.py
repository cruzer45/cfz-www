from cfzcore.person import Person
from cfzpersistence.repository.outlets import (OutletsRepository,
                                               OutletVehiclepassRepository)
from ..models import DBSession
from datetime import datetime
from cfzcore.outlet import OutletVehiclepass, Outlet
from cfzcore.address import Address


class OutletService(object):
    def __init__(self, outlets_repository=OutletsRepository(DBSession)):
        self.outlets_repository = outlets_repository

    def create_outlet(self, company, **kwargs):
        outlet = Outlet(kwargs['name'])
        outlet.start_date = datetime.strptime(kwargs.get('start_date'), "%d/%m/%Y")
        outlet.status = kwargs['status']
        outlet.company = company
        outlet.telephone_number = kwargs.get('telephone_number', None)
        outlet.fax_number = kwargs.get('fax_number', None)
        outlet.manager = self.create_manager(**kwargs)
        outlet.address = self.create_address(**kwargs)
        return self.persist(outlet)

    def create_manager(self, **kwargs):
        person = Person(kwargs.get('first_name'), kwargs.get('last_name'))
        person.phone1 = kwargs.get('phone1', None)
        person.phone2 = kwargs.get('phone2', None)
        return person

    def create_address(self, **kwargs):
        address = Address()
        address = address.update(kwargs.get('line1', None),
                                 kwargs.get('line2', None))
        return address

    def update(self, outlet, **kwargs):
        manager = outlet.manager
        manager.first_name = kwargs.get('first_name')
        manager.last_name = kwargs.get('last_name')
        manager.phone1 = kwargs.get('phone1')
        manager.phone2 = kwargs.get('phone2')
        outlet.manager = manager
        return outlet

    def persist(self, outlet):
        return self.outlets_repository.persist(outlet)

    def get_outlet(self, outlet_id):
        return self.outlets_repository.find_by_id(outlet_id)

    def persist_vehiclepass(self,
                            vehiclepass,
                            repository=OutletVehiclepassRepository(DBSession)):
        return repository.persist(vehiclepass)

    def add_vehiclepass(self, outlet, **kwargs):
        kwargs['outlet'] = outlet
        kwargs['date_issued'] = datetime.strptime(kwargs.get('date_issued'), "%d/%m/%Y")
        vehiclepass = OutletVehiclepass(**kwargs)
        vehiclepass.outlet = outlet
        return self.persist_vehiclepass(vehiclepass)

    def update_vehiclepass(self, outlet, **kwargs):
        vehiclepass = self._get_vehiclepass(kwargs.get('id'))
        vehiclepass.owner = kwargs.get('owner', vehiclepass.owner)
        vehiclepass.vehicle_year = kwargs.get('vehicle_year', vehiclepass.vehicle_year)
        vehiclepass.vehicle_model = kwargs.get('vehicle_model', vehiclepass.vehicle_model)
        vehiclepass.license_plate = kwargs.get('license_plate', vehiclepass.license_plate)
        vehiclepass.month_expired = kwargs.get('month_expired', vehiclepass.month_expired)
        vehiclepass.sticker_number = kwargs.get('sticker_number', vehiclepass.sticker_number)
        vehiclepass.expired = kwargs.get('expired', vehiclepass.expired)
        vehiclepass.issued_by = kwargs.get('issued_by', vehiclepass.issued_by)
        if 'date_issued' in kwargs:
            vehiclepass.date_issued  = datetime.strptime(kwargs.get('date_issued'), '%d/%m/%Y')
        vehiclepass.outlet = outlet
        return self.persist_vehiclepass(vehiclepass)

    def _get_vehiclepass(self,
                         vehiclepass_id,
                         repository=OutletVehiclepassRepository(DBSession)):
        return repository.get(vehiclepass_id)

