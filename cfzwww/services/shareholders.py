from datetime import datetime
from cfzcore.shareholder import Shareholder
from cfzpersistence.repository.shareholder import ShareholderRepository
from cfzwww.models import DBSession


class ShareholderCrudService(object):
	def __init__(self, repository=ShareholderRepository(DBSession)):
		self.repository = repository
		print "__dict__: ", self.__dict__

	def update(self, **kwargs):
		shareholder = self.repository.get(kwargs.get('id'))
		if shareholder.shares != kwargs.get('shares'):
			_shareholder = Shareholder(**kwargs)
			_shareholder.person = shareholder.person
			_shareholder.first_name = kwargs.get('first_name', shareholder.first_name)
			_shareholder.last_name = kwargs.get('last_name', shareholder.last_name)
			_shareholder.shares = kwargs.get('shares')
			_shareholder.company = shareholder.company
			shareholder.modified_date = datetime.now()
			shareholder.active = False
			self.repository.persist(shareholder)
			_shareholder = self.repository.persist(_shareholder)
			return _shareholder
		else:
			_shareholder = shareholder
			_shareholder.first_name = kwargs.get('first_name', shareholder.first_name)
			_shareholder.last_name = kwargs.get('last_name', shareholder.last_name)
			_shareholder.modified_date = datetime.now()
			_shareholder = self.repository.persist(_shareholder)
			return _shareholder
