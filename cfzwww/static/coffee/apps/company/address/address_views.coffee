@Company.module "CompanyAddress.Show", (Show, App, Backbone, Marionette, $, _)->

	class Show.EmptyAddressView extends Marionette.ItemView
		template: Handlebars.templates['address_empty']

		events:
			'click #save-address-btn': 'saveAddress'
			'click #address-return-btn': 'returnBtn'

		saveAddress: (e)->
			e.preventDefault()
			App.vent.trigger "address:model:save", {plaza: $('#plaza').val(), locale: $('#locale').val()}, @model

		returnBtn: (domEvent) ->
			domEvent.preventDefault()
			App.vent.trigger "address:form:cancel", @model

	class Show.AddressTable extends Marionette.ItemView
		template: Handlebars.templates['address_table']

		events:
			'click #edit-btn': 'showForm'

		showForm: (domEvent) ->
			domEvent.preventDefault()
			App.vent.trigger "address:edit:form", @model
