@Company = do (Backbone, Marionette) ->
	Marionette.Region.prototype.open = (view)->
		@$el.hide()
		@$el.html(view.el)
		#@$el.slideDown("normal")
		@$el.show()

	App = new Marionette.Application

	App.addRegions
		mainRegion: '#tab-content'
		navBarRegion: '#company-navbar'
		directorsRegion: '#tab-directors'
		directorsItemRegion: '#directors-tbody'
		spinnerRegion: '#spinner'


	App.addInitializer ->
		App.module('SpinnerController').start()
		App.vent.trigger "spinner:start", document.getElementById('spinner')
		App.module('Entities').start()
		fetchingCompanyTypes = App.reqres.request 'companyTypes:fetch'
		fetchingCompanyTypes.done (companyTypes)->
			App.companyTypes = companyTypes

	App.vent.on "entity:initialized", ->
		App.company = App.reqres.request "company:entity"
		App.vent.trigger "company:courtesypasses:initialize", App.company
		App.courtesypasses = App.reqres.request "courtesypasses:all"
		App.module('GoodsCategories.Entities').start()
		App.vent.trigger "spinner:stop"
		App.company = App.request "company:entity"
		fetchingRepresentationtypes = App.reqres.request "company:representationtypes:fetch"
		fetchingRepresentationtypes.done (representationtypes)->
			App.representationtypes = representationtypes
		App.startGeneralInformationApp App.company
		App.module('NavBarApp').start()
		App.module('CompanyContactsApp').start()
		App.module('CompanyAddress').start()
		App.module('Shareholder').start()
		App.module('AnnualReport').start()
		App.module('VehicleSticker').start()
		App.module('GoodsCategories').start()
		return

	App.vent.on "contactNumbers:request:show", ->
		App.execute "fetchContactNumbers", App.company.get('id')
		return

	App.vent.on "address:request:show", ->
		App.execute "fetchAddress", App.company.get('id')
		return

	App.vent.on "directors:request:show", ->
		App.execute "fetchDirectors", App.company
		return

	App.vent.on "shareholders:request:show", ->
		App.execute "fetchShareholders", App.company	
		return

	App.vent.on "reports:request:show", ->
		App.execute "fetchAnnualReports", App.company
		return

	App.vent.on "stickers:request:show", ->
		App.execute "fetchVehicleStickers", App.company
		return

	App.vent.on "outlets:request:show", ->
		App.execute "fetchOutlets", App.company
		return

	App.vent.on "contactNumbers:initialized", ->
		App.contactNumber = App.request "company:contactNumber:entity"
		App.vent.trigger "contactNumbers:show"

	App.vent.on "companyAddress:initialized", ->
		App.address = App.request "company:address:entity"
		App.vent.trigger "company:address:show"

	App.vent.on "companyDirectors:initialized", ->
		App.directors = App.request "company:directors:entities"
		App.vent.trigger "company:directors:list"

	App.vent.on "company:shareholders:initialized", ->
		App.shareholders = App.request "company:shareholders:entities"
		App.vent.trigger "company:shareholders:list"

	App.vent.on "company:reports:initialized", ->
		App.reports = App.request "company:reports:entities"
		App.vent.trigger "company:reports:list"

	App.vent.on "company:stickers:initialized", ->
		App.vehicle_stickers = App.request "company:stickers:entities"
		App.vent.trigger "company:stickers:list", App.vehicle_stickers

	App.vent.on "company:outlets:initialized", ->
		App.outlets = App.request "company:outlets:entities"
		App.vent.trigger "company:outlets:list"

	App.on "initialize:after", ->
		if Backbone.history
			Backbone.history.start()
			
	App.showGoodsCategories = ->
		App.execute "company:goodsCategories:show", App.company

	App.showCourtesyPass = ->
		App.execute "company:courtesyPass:show"
		return

	App.startGeneralInformationApp = (company)->
		App.module('OperationalContract').start()
		fetchingOpContracts = App.reqres.request "company:operationalContracts:fetch", company
		fetchingOpContracts.done (opcontracts)->
			App.operationContracts = opcontracts
			App.commands.execute "company:opcontracts:show"
		App.module('GeneralInformationApp').start()
		return

	App