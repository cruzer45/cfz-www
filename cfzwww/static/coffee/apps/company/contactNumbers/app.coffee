@Company.module "CompanyContactsApp", (CompanyContactsApp, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	class CompanyContactsApp.Controller extends Marionette.Controller
		initialize: ->
			if  @hasContactNumbers()
				@showContactsView()
			else
				@showContactsForm()
			App.vent.on "company:contactNumbers:save", (attributes, contactNumbers) => 
				@saveContactNumbers(attributes, contactNumbers)
			App.vent.on "company:contactNumbers:form:cancel", (address) => @showContactsView(address)
			App.vent.on "company:contactNumbers:edit:form", => @showContactsForm()

		show: (view) ->
			App.mainRegion.show view

		showContactsView: ->
			view = @getContactsView()
			@show(view)

		showContactsForm: ->
			view = @getContactsForm()
			@show(view)

		getContactsView: ->
			new CompanyContactsApp.Show.ContactNumbersView
				model: App.contactNumber

		getContactsForm: ->
			new CompanyContactsApp.Show.ContactNumbersForm
				model: App.contactNumber

		hasContactNumbers: ->
			!!App.contactNumber.get('fax_number') || !!App.contactNumber.get('telephone_number') || !!App.contactNumber.get('cell_number')

		saveMessage: ->
			"<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>Saved Company's Contact Numbers!</div>"

		errorMessage: ->
			"<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>Error while saving Company's Contact Numbers!</div>"

		saveContactNumbers: (attributes, contactNumbers) ->
			savingContactNumbers = contactNumbers.save(attributes)
			savingContactNumbers.done (contactInfo) =>
				App.contactNumber = contactNumbers
				@showContactsView()
				$('#alert-space').html @saveMessage()
			savingContactNumbers.fail (jqXHR) =>
				console.warn "Error saving contact numbers: ", jqXHR
				@errorMessage()

	App.vent.on "contactNumbers:show", ->
		new CompanyContactsApp.Controller
