@Company.module "CompanyContactsApp.Show", (Show, App, Backbone, Marionette, $, _)->

	class Show.ContactNumbersView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['contact_numbers']

		events:
			'click #edit-btn': 'showForm'

		showForm: (domEvent) ->
			domEvent.preventDefault()
			App.vent.trigger "company:contactNumbers:edit:form", @model

	class Show.ContactNumbersForm extends Marionette.ItemView
		template: Handlebars.templates['contact_numbers_form']

		events:
			'click #save-contact-numbers-btn': 'saveContactNumbers'
			'click #contact-numbers-return-btn': 'returnBtn'

		saveContactNumbers: (e)->
			e.preventDefault()
			e.stopPropagation()
			attributes = {telephone_number: $('#telephone_number').val(), fax_number: $('#fax_number').val(), cell_number: $('#cell_number').val()}
			App.vent.trigger "company:contactNumbers:save", attributes, @model

		returnBtn: (domEvent) ->
			domEvent.preventDefault()
			App.vent.trigger "company:contactNumbers:form:cancel", @model

