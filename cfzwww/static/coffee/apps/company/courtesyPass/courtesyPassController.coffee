@Company.module "CourtesyPass.Controller", (Controller, App, Backbone, Marionette, $, _)->

	API =
		show: ->
			layout = @getLayout()
			App.mainRegion.show layout
			layout.courtesyPassListRegion.show new App.CourtesyPass.Views.CollectionView(collection: App.courtesypasses)
			layout.createBtnRegion.show new App.CourtesyPass.Views.CreateButton

		getLayout: ->
			new App.CourtesyPass.Views.CourtesyPassLayout

		saveCourtesypass: (courtesypass)->
			promise = courtesypass.save()
			promise.done((data, textStatus, jqXHR)=>
				bootbox.alert("<h4 class='alert alert-success alert-block'>Saved Courtesy Pass!</h4>", ->
					App.vent.trigger "spinner:stop"
					App.commands.execute "courtesypasses:add", new App.Entities.CourtesyPass(data)
					App.commands.execute "company:courtesyPass:show"
				)
			)
			promise.fail((options)=>
				bootbox.alert("<h4 class='alert alert-error alert-block'>Failed to save Courtesy Pass!</h4>", ->
					console.log "Failed to save courtesy pass: ", options.statusText
					App.vent.trigger "spinner:stop"
					App.commands.execute "company:courtesyPass:show"
				)
			)
			courtesypass

		showForm: (courtesypass)->
			App.mainRegion.show new App.CourtesyPass.Views.Form(model: courtesypass)

	App.commands.addHandler "company:courtesyPass:show", ->
		API.show() 

	App.commands.addHandler "company:show:courtesypassForm", (courtesypass)->
		courtesypass.set('company_id', App.company.get('id'))
		API.showForm courtesypass

	App.commands.addHandler "company:courtesyPass:save", (courtesypass)->
		App.vent.trigger "spinner:start"
		savedCourtesypass = API.saveCourtesypass courtesypass
		savedCourtesypass

