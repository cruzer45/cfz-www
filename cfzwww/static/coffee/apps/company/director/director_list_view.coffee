@Company.module "CompanyDirector.List", (List, App, Backbone, Marionette, $, _)->

	class List.DirectorCollectionView extends Backbone.Marionette.CompositeView
		itemView: List.DirectorView
		itemViewContainer: "tbody"
		template: Handlebars.templates['directors_list_header']

		events:
			'click #add-director-btn': 'addDirectorForm'

		addDirectorForm: ->
			App.vent.trigger 'company:show:newDirectorForm'
	
