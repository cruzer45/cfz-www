@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
	@startWithParent = false

	class Entities.Company extends Backbone.Model
		urlRoot: '/company'

	class Entities.AcceptanceLetter extends Backbone.Model
		url: ->
			"/company/#{@get('id')}/letter_of_acceptance"

	API =
		fetchCompany: ->
			locationpath = location.pathname.split('/')
			companyId = locationpath[locationpath.length - 1]
			API.company = new Entities.Company()
			API.company.set('id', companyId)
			API.company.fetch().done(->
				App.vent.trigger "entity:initialized" 
			)

		getCompany: ->
			API.company

		setCompany: (company)->
			API.company = company

	App.reqres.addHandler "company:entity", ->
		API.getCompany()

	App.commands.addHandler "company:update", (company)->
		API.setCompany(company)

	Entities.on "start", ->
		API.fetchCompany()

