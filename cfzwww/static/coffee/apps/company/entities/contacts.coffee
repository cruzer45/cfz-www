@Company.module "Entities.ContactNumber", (ContactNumber, App, Backbone, Marionette, $, _) ->
	class ContactNumber.ContactNumber extends Backbone.Model
		url: ->
			"/company/#{@attributes.id}/contact_numbers"

	API = 
		fetch: (companyId)->
			API.contactNumber = new ContactNumber.ContactNumber('id': companyId)
			API.contactNumber.fetch().done(->
				App.vent.trigger "contactNumbers:initialized"
			)
			return

		get: ->
			return API.contactNumber

	App.reqres.addHandler "company:contactNumber:entity", ->
		API.get()

	App.commands.addHandler "fetchContactNumbers", (companyId)->
		API.fetch(companyId)

