@Company.module "GoodsCategories.Entities", (Entities, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	class Entities.GoodsCategory extends Backbone.Model
		defaults:
			name: ''

	class Entities.CompanyGoodsCategory extends Entities.GoodsCategory
		urlRoot: ->
			"/company/#{@company.get('id')}/good"
			
		defaults:
			name: ''

		initialize: (options)->
			@setCompany(options.company)

		setCompany: (company)->
			@company = company

	class Entities.GoodsCategories extends Backbone.Collection
		model: Entities.GoodsCategory
		url: '/goods'

	class Entities.CompanyGoodsCategories extends Backbone.Collection
		model: Entities.CompanyGoodsCategory
		url: ->
			"/company/#{@company.get('id')}/goods"

		setCompany: (company)=>
			@company = company


	API =
		fetchGoodsCatgories: ->
			@_goodsCategories = new Entities.GoodsCategories()
			selectMessage = new Entities.GoodsCategory
								id: '0'
								name: 'Select A Category'
			promise = @_goodsCategories.fetch()
			promise.done( =>
				@_goodsCategories.add selectMessage, {at: 0}
			)
			promise.fail( ->
				console.log "did not retrieve goods"
			)
			@_goodsCategories

		fetchCompanyGoodsCategories: (company)->
			company = App.reqres.request "company:entity"
			@_companyGoodsCategories = new Entities.CompanyGoodsCategories
			@_companyGoodsCategories.setCompany company
			@_companyGoodsCategories.fetch()
			@_companyGoodsCategories

		getGoodsCategories: ->
			@_goodsCategories

		getCompanyGoodsCategories: (company)->
			@_companyGoodsCategories


	App.reqres.addHandler "company:goodsCategories:all", (company)->
		API.getCompanyGoodsCategories(company)

	App.reqres.addHandler "goodsCategories:all", ->
		API.getGoodsCategories()

	App.commands.addHandler "company:goodsCategories:fetch", ->
		API.fetchGoodsCatgories()
		API.fetchCompanyGoodsCategories()

	Entities.on("start", ->
		API.fetchGoodsCatgories()
		API.fetchCompanyGoodsCategories(App.company)
	)
