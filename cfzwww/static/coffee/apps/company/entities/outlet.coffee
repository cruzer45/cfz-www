@Company.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

	class Entities.Outlet extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/outlet"

		defaults:
			start_date: moment(new Date()).format("DD/MM/YYYY")
			links: ''
			first_name: ''
			last_name: ''
			telephone_number: ''
			fax_number: ''
			line1: ''
			line2: ''

	class Entities.Outlets extends Backbone.Collection
		url: ->
			"/company/#{@company.id}/outlets"

		initialize: (company)->
			@company = company

	API =
		fetchAll: (company)->
			API.outlets = new Entities.Outlets(company)
			API.outlets.fetch().done(->
				App.vent.trigger "company:outlets:initialized"
			)

		getAll: ->
			API.outlets

	App.reqres.addHandler "company:outlets:entities", ->
		API.getAll()

	App.commands.addHandler "fetchOutlets", (company)->
		API.fetchAll(company)
