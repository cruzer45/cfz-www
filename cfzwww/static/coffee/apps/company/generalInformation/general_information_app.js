// Generated by CoffeeScript 1.6.3
this.Company.module("GeneralInformationApp", function(GeneralInformationApp, App, Backbone, Marionette, $, _) {
  var API;
  this.startWithParent = false;
  API = {
    show: function(company, authorizedpersons) {
      var createBtnView, listView;
      this.generalInformationView = this._createGeneralInformationView(company);
      console.log("generalInformationView: ", this.generalInformationView);
      App.mainRegion.show(this.generalInformationView);
      this.authorizedPersonsLayout = this._createAuthorizedPersonsView();
      this.generalInformationView.authorizedPersons.show(this.authorizedPersonsLayout);
      listView = new App.AuthorizedPersons.Views.List({
        collection: authorizedpersons
      });
      createBtnView = new App.AuthorizedPersons.Views.CreateButton;
      this.authorizedPersonsLayout.authorizedPersonsListRegion.show(listView);
      return this.authorizedPersonsLayout.createBtnRegion.show(createBtnView);
    },
    _createAuthorizedPersonsView: function() {
      var layout;
      layout = new App.AuthorizedPersons.Views.Layout;
      return layout;
    },
    _createGeneralInformationView: function(company) {
      this.generalInformationView = new GeneralInformationApp.Show.GeneralInformationView({
        model: company
      });
      return this.generalInformationView;
    },
    getAuthorizedPersonsLayout: function() {
      return this.authorizedPersonsLayout;
    },
    getGeneralInformationView: function() {
      return this.generalInformationView;
    },
    fetchCompany: function() {
      var companyId, locationpath;
      locationpath = location.pathname.split('/');
      companyId = locationpath[locationpath.length - 1];
      API.company = new App.Entities.Company();
      API.company.set('id', companyId);
      API.company.fetch();
      return API.company;
    },
    showAuthorizedPersonsForm: function(model, representationtypes) {
      var formView;
      formView = new App.AuthorizedPersons.Views.Form({
        model: model,
        representationtypes: representationtypes
      });
      return this.getGeneralInformationView().authorizedPersons.show(formView);
    },
    showAuthorizedPersonsList: function(authorizedpersons) {
      var createBtnView, listView;
      listView = new App.AuthorizedPersons.Views.List({
        collection: authorizedpersons
      });
      createBtnView = new App.AuthorizedPersons.Views.CreateButton;
      this.getGeneralInformationView().authorizedPersons.show(this.getAuthorizedPersonsLayout());
      this.getAuthorizedPersonsLayout().authorizedPersonsListRegion.show(listView);
      return this.getAuthorizedPersonsLayout().createBtnRegion.show(createBtnView);
    },
    saveAuthorizedPerson: function(authorizedperson) {
      return authorizedperson.save();
    },
    addAuthorizedPersonToCollection: function(authorizedperson, collection) {
      collection.add(authorizedperson);
      return collection;
    }
  };
  App.vent.on("generalInformation:show", function() {
    API.show(App.company, App.authorizedpersons, App.operationContracts);
    if (App.operationContracts) {
      return App.commands.execute("company:opcontracts:show");
    }
  });
  App.commands.addHandler("company:opcontracts:show", function() {
    return App.commands.execute("opcontracts:show", App.operationContracts, API.getGeneralInformationView());
  });
  App.vent.on("company:authorizedperson:showForm", function(model) {
    return API.showAuthorizedPersonsForm(model, App.representationtypes);
  });
  App.vent.on("company:authorizedpersons:showList", function(authorizedpersons) {
    return API.showAuthorizedPersonsList(authorizedpersons);
  });
  App.commands.addHandler("company:authorizedperson:save", function(authorizedperson) {
    var promise,
      _this = this;
    App.vent.trigger("spinner:start");
    promise = API.saveAuthorizedPerson(authorizedperson);
    promise.done(function(data, textStatus, jqXHR) {
      return bootbox.alert("<h4 class='alert alert-success alert-block'>Saved Authorized Person!</h4>", function() {
        var authorizedpersons, newAuthorizedPerson;
        App.vent.trigger("spinner:stop");
        newAuthorizedPerson = new App.Entities.AuthorizedPerson(data);
        authorizedpersons = API.addAuthorizedPersonToCollection(newAuthorizedPerson, App.authorizedpersons);
        return API.showAuthorizedPersonsList(authorizedpersons);
      });
    });
    return promise.fail(function(options) {
      return bootbox.alert("<h4 class='alert alert-error alert-block'>Failed to save Authorized Person!</h4>", function() {
        App.vent.trigger("spinner:stop");
        return API.showAuthorizedPersonsList(App.authorizedpersons);
      });
    });
  });
  return GeneralInformationApp.on('start', function() {
    var fetchedAuthorizedPersons;
    API.show(App.company, App.authorizedpersons, App.operationContracts);
    fetchedAuthorizedPersons = App.reqres.request("company:authorizedpersons:fetch", App.company);
    return fetchedAuthorizedPersons.done(function(authorizedpersons) {
      App.authorizedpersons = authorizedpersons;
      return API.showAuthorizedPersonsList(App.authorizedpersons);
    });
  });
});
