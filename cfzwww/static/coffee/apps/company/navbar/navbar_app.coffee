@Company.module "NavBarApp", (NavBarApp, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		show: ->
			App.navBarRegion.show(new NavBarApp.CompanyInformationNavBar())

	NavBarApp.on "start", ->
		API.show()

