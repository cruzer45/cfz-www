@Company.module "NavBarApp", (NavBarApp, App, Backbone, Marionette, $, _)->

	class NavBarApp.CompanyInformationNavBar extends Marionette.ItemView
		template: Handlebars.templates['company_nav']
		tagName:  'ul'
		className: 'nav nav-tabs tabs-two'

		events:
			'click #generalInfo-tab': 'showGeneralInfo'
			'click #contactNumbers-tab': 'showContactNumbers'
			'click #address-tab': 'showAddress'
			'click #director-tab': 'showDirectors'
			'click #shareholders-tab': 'showShareholders'
			'click #annualreports-tab': 'showAnnualReports'
			'click #stickers-tab': 'showVehicleStickers'
			'click #outlets-tab': 'showOutlets'
			'click #goods-categories-tab': 'showGoodsCategories'
			'click #courtesy-pass-tab': 'showCourtesyPassList'

		showGeneralInfo: ->
			App.vent.trigger "generalInformation:show"
			@setAsActive('#generalInfo-tab-item')

		showContactNumbers: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "contactNumbers:request:show"
			@setAsActive('#contactNumbers-tab-item')
			App.vent.trigger "spinner:stop"

		showAddress: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "address:request:show"
			@setAsActive('#address-tab-item')
			App.vent.trigger "spinner:stop"

		showDirectors: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "directors:request:show"
			@setAsActive('#director-tab-item')
			App.vent.trigger "spinner:stop"

		showShareholders: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "shareholders:request:show"
			@setAsActive('#shareholders-tab-item')
			App.vent.trigger "spinner:stop"

		showAnnualReports: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "reports:request:show"
			@setAsActive('#annualreports-tab-item')
			App.vent.trigger "spinner:stop"

		showVehicleStickers: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "stickers:request:show"
			@setAsActive('#stickers-tab-item')
			App.vent.trigger "spinner:stop"

		showOutlets: ->
			App.vent.trigger "spinner:start"
			App.vent.trigger "outlets:request:show"
			@setAsActive('#outlets-tab-item')
			App.vent.trigger "spinner:stop"

		showGoodsCategories: ->
			App.vent.trigger "spinner:start"
			@setAsActive('#goods-categories-tab-item')
			App.showGoodsCategories()
			App.vent.trigger "spinner:stop"

		showCourtesyPassList: ->
			App.vent.trigger "spinner:start"
			@setAsActive('#courtesy-pass-tab-item')
			App.showCourtesyPass()
			App.vent.trigger "spinner:stop"
			
		setAsActive: (id) ->
			listItems = [
				'#generalInfo-tab-item',
				'#contactNumbers-tab-item',
				'#address-tab-item',
				'#director-tab-item',
				'#shareholders-tab-item',
				'#annualreports-tab-item',
				'#stickers-tab-item',
				'#goods-categories-tab-item',
				'#outlets-tab-item',
				"#courtesy-pass-tab-item"
			]
			for item in listItems
				if id == item
					$(item).toggleClass('active', true)
				else
					$(item).toggleClass('active', false)
