@Company.module "OperationalContract.Views", (Views, App, Backbone, Marionette, $, _)->

	class Views.Layout extends Marionette.Layout
		template: Handlebars.templates['opcontracts_layout']

		regions:
			'createBtnRegion': '#create-opcontract-btn'
			'contractsRegion': '#opcontracts-list'


	class Views.OpContractRow extends Marionette.ItemView
		template: Handlebars.templates['opcontracts_list']
		tagName: 'tr'

		events:
			'click #view-btn': 'showContract'

		showContract: (e)->
			e.preventDefault()
			window.location = "/operational_contract/#{@model.get('id')}"



	class Views.NoOpConracts extends Marionette.ItemView
		template: Handlebars.templates['opcontracts_empty']
		tagName: 'tr'

	class Views.CreateButton extends Marionette.ItemView
		template: Handlebars.templates['opcontracts_createbtn']
		tagName: 'span'

		events:
			'click #add-contract-btn': 'createContract'

		createContract: (domEvent)->
			domEvent.preventDefault()
			opcontract = new App.Entities.OperationalContract()
			opcontract.set('company_id', App.company.get('id'))
			App.commands.execute "company:operationalcontract:save", opcontract

	class Views.List extends Marionette.CompositeView
		template: Handlebars.templates['opcontracts_header']
		itemViewContainer: 'tbody'
		itemView: Views.OpContractRow
		emptyView: Views.NoOpConracts


