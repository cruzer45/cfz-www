@Company.module "Shareholder", (Shareholder, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		showListView: ->
			view = @getListView()
			App.mainRegion.show(view)

		getListView: ->
			new Shareholder.ShareholderCollectionView
				collection: App.shareholders

		showForm: (shareholder) ->
			App.mainRegion.show(new Shareholder.ShareholderFormView(model: shareholder))

	App.vent.on "company:shareholders:list", ->
		API.showListView()

	App.vent.on "company:show:shareholderNewForm", ->
		shareholder = new App.Entities.Shareholder()
		shareholder.set('companyId', App.company.get('id'))
		API.showForm(shareholder)

	App.vent.on "company:show:shareholderForm", (shareholder)->
		shareholder.set('companyId', App.company.get('id'))
		API.showForm(shareholder)
