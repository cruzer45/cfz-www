@Company.module "SpinnerController", (SpinnerController, App, Backbone, Marionette, $, _)->

	API = 
		opts: 
			lines: 13
			length: 7
			width: 4
			radius: 10
			corners: 1
			rotate: 0
			color: '#000'
			speed: 1
			trail: 60
			shadow: false
			hwaccel: false
			className: 'spinner'
			zIndex: 9e9
			top: 'auto'
			left: "auto"

		createSpinner: ->
			@spinner = new Spinner(@opts)

		startSpinner: (targetElement)->
			@spinner.spin()
			targetElement.appendChild(@spinner.el)

		stopSpinner: ->
			@spinner.stop()

	SpinnerController.on 'start', ->
		API.createSpinner()

	App.vent.on "spinner:start", (targetElement=document.getElementById('spinner'))->
		API.startSpinner(targetElement)

	App.vent.on "spinner:stop", ->
		API.stopSpinner()
