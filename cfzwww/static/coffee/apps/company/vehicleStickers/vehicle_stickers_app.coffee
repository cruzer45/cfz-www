@Company.module "VehicleSticker", (VehicleSticker, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	API =
		showListView: (vehiclepass_list)->
			view = @getLayout()
			App.mainRegion.show(view)
			view.stickersListRegion.show(@getListView())
			view.createBtnRegion.show(@createStickersCreateButton(vehiclepass_list))
			max_vehicles = new App.Entities.MaxVehicleStickers
				id: App.company.get('id')
				max_vehicles: App.company.get('max_vehicles')
			max_vehicles_form = new VehicleSticker.VehicleStickerMaxPassesForm
				model: max_vehicles
			view.maxStickersFormRegion.show max_vehicles_form

		getListView: ->
			new VehicleSticker.VehicleStickersCollectionView
				collection: App.vehicle_stickers

		showForm: (vehicle_sticker) ->
			App.mainRegion.show(new VehicleSticker.VehicleStickerFormView(model: vehicle_sticker))

		getLayout: ->
			new VehicleSticker.VehicleStickerLayout

		createStickersCreateButton: (vehiclepass_list)->
			new VehicleSticker.VehicleStickerCreateButton
				collection: vehiclepass_list

		hasAvailablePasses: (non_expired_vehiclepasses, max_vehicles)->
			max_vehicles > non_expired_vehiclepasses.length


	App.vent.on "company:stickers:list", (vehiclepass_list)->
		API.showListView vehiclepass_list

	App.vent.on "company:show:stickerNewForm",(vehiclepass_list, max_vehicles) ->
		vehicle_sticker = new App.Entities.VehicleSticker()
		vehicle_sticker.set('companyId', App.company.get('id'))
		non_expired_vehiclepasses = App.reqres.request "vehiclepass:not_expired:list", vehiclepass_list
		if API.hasAvailablePasses(non_expired_vehiclepasses, max_vehicles)
			API.showForm(vehicle_sticker)
		else
			bootbox.alert('<h4>Company has reached maximum number of vehcilepasses!</h4>')

	App.vent.on "company:show:stickerEditForm", (vehicle_sticker)->
		vehicle_sticker.set('companyId', App.company.get('id'))
		API.showForm(vehicle_sticker)


