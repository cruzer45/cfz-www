@Company.module "VehicleSticker", (VehicleSticker, App, Backbone, Marionette, $, _)->

	class VehicleSticker.VehicleStickerView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['vehiclepass_list']
		tagName: 'tr'

		events:
			'click #edit-vehiclepass-btn': 'editVehiclepass'

		editVehiclepass: ->
			App.vent.trigger "company:show:stickerEditForm", @model


	class VehicleSticker.VehicleStickersCollectionView extends Backbone.Marionette.CompositeView
		template: Handlebars.templates['vehiclepass_header']
		itemViewContainer: 'tbody'
		itemView: VehicleSticker.VehicleStickerView


	class VehicleSticker.VehicleStickerCreateButton extends Backbone.Marionette.ItemView
		template: Handlebars.templates['vehiclepass_create_btn']
		tagName: 'span'

		events:
			'click #add-sticker-btn': 'showForm'

		showForm: ->
			vehiclepass_list = App.request 'company:stickers:entities'
			_company = App.reqres.request 'company:entity'
			max_vehicles = _company.get('max_vehicles')
			App.vent.trigger 'company:show:stickerNewForm', vehiclepass_list, max_vehicles


	class VehicleSticker.VehicleStickerMaxPassesForm extends Backbone.Marionette.ItemView
		template: Handlebars.templates['vehiclepass_maxnumber_form']
		tagName: 'span'

		events:
			'click #save-max-passes-btn': 'saveMaxPasses'

		saveMaxPasses: (e)->
			e.preventDefault()
			max_vehicles = $('#max-vehicles').val()
			App.commands.execute "company:add:vehiclepass", @model, max_vehicles


	class VehicleSticker.VehicleStickerLayout extends Backbone.Marionette.Layout
		template: Handlebars.templates['vehiclepass_layout']

		regions:
			'createBtnRegion': '#create-vehiclepass-btn'
			'maxStickersFormRegion': '#max-vehiclepass-form'
			'stickersListRegion': '#vehiclepass-list'


	class VehicleSticker.VehicleStickerFormView extends Backbone.Marionette.ItemView
		template: Handlebars.templates['vehiclepass_form']

		events: 
			'click #cancel-sticker-btn': 'cancelForm'
			'click #save-sticker-btn': 'saveSticker'
			'keyup #vehicle-owner': 'requiredFieldAlert'
			'keyup #vehicle-model': 'requiredFieldAlert'
			'keyup #vehicle-year': 'requiredFieldAlert'
			'keyup #vehicle-color': 'requiredFieldAlert'
			'keyup #license-plate': 'requiredFieldAlert'
			'keyup #sticker-number': 'requiredFieldAlert'
			'keyup #receipt-number': 'requiredFieldAlert'

		onRender: ->
			_.defer((args)->
				model = args[0]
				$('#vehicle-owner-alert').hide()
				$('#vehicle-year-alert').hide()
				$('#vehicle-model-alert').hide()
				$('#vehicle-color-alert').hide()
				$('#sticker-number-alert').hide()
				$('#date-issued-alert').hide()
				$('#license-plate-alert').hide()
				$('#receipt-number-alert').hide()

				if _.isUndefined(model.get('id'))
					$('#save-sticker-btn').attr("disabled", "disabled")

			, [@model])

		onShow: ->
			$("#date-issued").datepicker
				showWeek: true
				firstDay: 1
				changeMonth: true
				changeYear: true
				showOtherMonths: true
				format: "dd/mm/yyyy"
				startDate: "01/01/1994"
			.on('changeDate', (ev)->
				$(@).change()
			)

			if @model.get('issued_by')
				$('#issued-by').show()
			else
				$('#issued-by').hide()

		initialize: ->
			@validationErrors = []

		cancelForm: (e)->
			e.preventDefault()
			App.vent.trigger "stickers:request:show"

		saveSticker: (e)->
			e.preventDefault()
			owner = $('#vehicle-owner').val()
			vehicle_year = $('#vehicle-year').val()
			vehicle_model = $('#vehicle-model').val()
			vehicle_color = $('#vehicle-color').val()
			license_plate = $('#license-plate').val()
			color = $('#vehicle-color').val()
			sticker_number = $('#sticker-number').val()
			date_issued = $('#date-issued').val()
			receipt_number = $('#receipt-number').val()
			month_expired = $('#month-expired').val()
			@model.save({
				owner: owner,
				vehicle_year: vehicle_year,
				vehicle_model: vehicle_model,
				color: color,
				license_plate: license_plate,
				sticker_number: sticker_number,
				date_issued: date_issued,
				receipt_number: receipt_number,
				month_expired: month_expired},
				{success: (model, response, options)->
					bootbox.alert('Vehicle Sticker Saved!', ->
						App.vent.trigger "stickers:request:show"
					)
				})

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-sticker-btn').attr("disabled", "disabled")
			else
				$('#save-sticker-btn').removeAttr("disabled")

