@GoodsCategories = do (Backbone, Marionette)->

	Application = new Marionette.Application

	class Application.Controller extends Marionette.Controller
		initialize: (goods_categories) ->
			@setGoodsCategories(goods_categories)
			@listenOnAddingGoodCategory()
			@listenOnSavingGoodCategory()

		showList: (goods_categories)->
			@setGoodsCategories goods_categories
			list = new Application.GoodsCategories.Views.Table
				collection: @getGoodsCategories()
			Application.listRegion.show list

		setGoodsCategories: (goods_categories) ->
			@goods_categories = goods_categories

		getGoodsCategories: ->
			@goods_categories

		showCreateForm: ->
			createForm = new Application.GoodsCategories.Views.CreateForm model: new Application.Entities.GoodsCategory
			Application.formRegion.show createForm

		hasGoodCategoryWithName: (good_category, limit=0) ->
			filterName = (good) -> 
				good.get('name') == good_category.get('name')
			duplicate_names = _.filter(@getGoodsCategories().models, filterName)
			duplicate_names.length > limit 

		hasGoodCategory: (good_category, limit=0) ->
			filterName = (good) -> good.get('id') == good_category.id
			duplicate_names = _.filter(@getGoodsCategories().models, filterName)
			duplicate_names.length > limit 
 
		listenOnAddingGoodCategory: ->
			Application.commands.addHandler 'goodsCategories:add', (goodsCategory) =>
				if @hasGoodCategoryWithName(goodsCategory)
					console.warn "This Good Category already exists! ", goodsCategory
					@failMessage "The Good Category  #{goodsCategory.get('name')} already exists!"
				else
					promise = goodsCategory.save()
					promise.done( (category) =>
						if !@hasGoodCategory(category)
							@getGoodsCategories().add(goodsCategory)
						@successMessage()
					)
					promise.fail (jqXHR) =>
						console.error "Error while saving Good Category: ", jqXHR
						@failMessage "An error occurred when saving the Good Category!"

		listenOnSavingGoodCategory: ->
			Application.commands.addHandler 'goodsCategories:save', (goodsCategory) =>
				if @hasGoodCategoryWithName(goodsCategory, 1)
					console.warn "This Good Category already exists! ", goodsCategory
					@failMessage "The Good Category  #{goodsCategory.get('name')} already exists!"
				else
					promise = goodsCategory.save()
					promise.done( (category) =>
						if !@hasGoodCategory(category)
							@getGoodsCategories().add(goodsCategory)
						@successMessage()
					)
					promise.fail (jqXHR) =>
						console.error "Error while saving Good Category: ", jqXHR
						@failMessage "An error occurred when saving the Good Category!"

		successMessage: ->
			messageString = "Saved Goods Category!"
			@showMessage(@message('alert-info', messageString))

		failMessage: (messageString) ->
			@showMessage(@message('alert-error', messageString))

		message: (cssClass, messageString) ->
			"<div class='alert #{cssClass}'><button type='button' class='close' data-dismiss='alert'>&times;</button>#{messageString}</div>"

		showMessage: (message) ->
			$('#messages-region').html(message)

	Application.addRegions
		listRegion: 		'#list-region'
		formRegion: 		'#form-region'
		messagesRegion: '#messages-region'

	Application.addInitializer ->
		Application.module('GoodsCategories.Views').start()
		@controller = new Application.Controller
		@controller.showCreateForm()

	Application.addInitializer ->
		Application.module('Entities').start()
		fetchingGoodsCategories = Application.reqres.request 'goodsCategories:fetch'
		fetchingGoodsCategories.done (goodsCategories)=>
			Application.goodsCategories = goodsCategories
			@controller.showList goodsCategories

	Application
