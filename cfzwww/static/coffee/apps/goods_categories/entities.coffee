@GoodsCategories.module "Entities", (Entities, Application, Backbone, Marionette, $, _)->
	@startWithParent = false

	class Entities.GoodsCategory extends Backbone.Model
		defaults:
			name: ''
		urlRoot: ->
			'/good_category'

	class Entities.GoodsCategories extends Backbone.Collection
		model: Entities.GoodsCategory
		url: '/goods'

	API =
		fetchGoodsCategories: ->
			defer = $.Deferred()
			@goodsCategories = new Entities.GoodsCategories()
			@goodsCategories.fetch
				success: =>
					defer.resolve(@goodsCategories)
			defer.promise()


	Application.reqres.addHandler "goodsCategories:fetch", ->
		API.fetchGoodsCategories()

