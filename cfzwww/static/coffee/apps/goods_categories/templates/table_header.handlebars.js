(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['table_header'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<table class=\"display table table-striped table-bordered\">\n   <thead>\n      <tr>\n         <th role=\"Name\">Name</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody></tbody>\n</table>\n\n";
  });
})();