// Generated by CoffeeScript 1.6.3
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

this.Outlet.module("Address", function(Address, App, Backbone, Marionette, $, _) {
  var _ref;
  this.startWithParent = false;
  Address.Controller = (function(_super) {
    __extends(Controller, _super);

    function Controller() {
      _ref = Controller.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Controller.prototype.initialize = function(options) {
      var _this = this;
      if (options == null) {
        options = {};
      }
      this.region = options.region;
      this.address = options.address;
      this.showView();
      App.vent.on("outlet:address:edit:form", function() {
        return _this.showForm();
      });
      App.vent.on("outlet:address:form:cancel", function() {
        return _this.showTable();
      });
      return App.commands.addHandler("outlet:address:save", function(address, attributes) {
        return _this.saveAddress(address, attributes);
      });
    };

    Controller.prototype.getFormView = function() {
      return new Address.Views.Form({
        model: this.address
      });
    };

    Controller.prototype.getTableView = function() {
      return new Address.Views.Table({
        model: this.address
      });
    };

    Controller.prototype.showForm = function() {
      var _ref1;
      if ((_ref1 = this.currentView) != null) {
        _ref1.close();
      }
      this.currentView = this.getFormView();
      return this.show(this.currentView);
    };

    Controller.prototype.showTable = function() {
      var _ref1;
      if ((_ref1 = this.currentView) != null) {
        _ref1.close();
      }
      this.currentView = this.getTableView();
      return this.show(this.currentView);
    };

    Controller.prototype.show = function(view) {
      this.listenTo(view, "close", this.close);
      return this.region.show(view);
    };

    Controller.prototype.showView = function() {
      if (this.hasAddress()) {
        return this.showTable();
      } else {
        return this.showForm();
      }
    };

    Controller.prototype.hasAddress = function() {
      var _ref1, _ref2;
      return !!((_ref1 = this.address) != null ? _ref1.get('line1') : void 0) || ((_ref2 = this.address) != null ? _ref2.get('line2') : void 0);
    };

    Controller.prototype.saveMessage = function() {
      return "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>Saved Outlet's Address!</div>";
    };

    Controller.prototype.errorMessage = function() {
      return "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>Error while saving Outlet's Address!</div>";
    };

    Controller.prototype.saveAddress = function(address, attributes) {
      var savingAddress,
        _this = this;
      savingAddress = address.save(attributes);
      savingAddress.done(function(_address) {
        App.contactNumber = _address;
        _this.showTable();
        return $('#alert-space').html(_this.saveMessage());
      });
      return savingAddress.fail(function(jqXHR) {
        console.warn("Error saving address: ", jqXHR);
        return _this.errorMessage();
      });
    };

    return Controller;

  })(Marionette.Controller);
  return Address.on("start", function() {
    return this.controller = new Address.Controller({
      region: App.mainRegion,
      address: App.address
    });
  });
});
