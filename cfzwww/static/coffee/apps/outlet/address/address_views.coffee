@Outlet.module "Address.Views", (Views, App, Backbone, Marionette, $, _)->

  class Views.Form extends Marionette.ItemView
    template: Handlebars.templates['address_form']

    events:
      'click #save-address-btn': 'saveAddress'
      'click #address-return-btn': 'returnBtn'

    saveAddress: (e)->
      e.preventDefault()
      line1 = $('#line1').val()
      line2 = $('#line2').val()
      attributes = {line1: line1, line2: line2}
      App.execute "outlet:address:save", @model, attributes

    returnBtn: (domEvent) ->
      domEvent.preventDefault()
      App.vent.trigger "outlet:address:form:cancel"

  class Views.Table extends Marionette.ItemView
    template: Handlebars.templates['address_table']

    events:
      'click #edit-btn': 'showForm'

    showForm: (domEvent) ->
      domEvent.preventDefault()
      App.vent.trigger "outlet:address:edit:form", @model
