// Generated by CoffeeScript 1.6.3
var __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

this.Outlet.module("Address.Views", function(Views, App, Backbone, Marionette, $, _) {
  var _ref, _ref1;
  Views.Form = (function(_super) {
    __extends(Form, _super);

    function Form() {
      _ref = Form.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Form.prototype.template = Handlebars.templates['address_form'];

    Form.prototype.events = {
      'click #save-address-btn': 'saveAddress',
      'click #address-return-btn': 'returnBtn'
    };

    Form.prototype.saveAddress = function(e) {
      var attributes, line1, line2;
      e.preventDefault();
      line1 = $('#line1').val();
      line2 = $('#line2').val();
      attributes = {
        line1: line1,
        line2: line2
      };
      return App.execute("outlet:address:save", this.model, attributes);
    };

    Form.prototype.returnBtn = function(domEvent) {
      domEvent.preventDefault();
      return App.vent.trigger("outlet:address:form:cancel");
    };

    return Form;

  })(Marionette.ItemView);
  return Views.Table = (function(_super) {
    __extends(Table, _super);

    function Table() {
      _ref1 = Table.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    Table.prototype.template = Handlebars.templates['address_table'];

    Table.prototype.events = {
      'click #edit-btn': 'showForm'
    };

    Table.prototype.showForm = function(domEvent) {
      domEvent.preventDefault();
      return App.vent.trigger("outlet:address:edit:form", this.model);
    };

    return Table;

  })(Marionette.ItemView);
});
