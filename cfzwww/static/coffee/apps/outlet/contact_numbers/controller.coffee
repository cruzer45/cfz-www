@Outlet.module "ContactNumber", (ContactNumber, App, Backbone, Marionette, $, _)->
  @startWithParent = false

  class ContactNumber.Controller extends Marionette.Controller

    initialize: (options={}) ->
      @region = options.region
      @contactNumbers = options.contactNumbers
      @showView()
      App.vent.on "outlet:contactNumbers:edit:form", => @showForm()
      App.vent.on "outlet:contactNumbers:form:cancel", => @showTable()
      App.commands.addHandler "outlet:contactNumbers:save", (contactNumbers, attributes) => 
        @saveContactNumbers(contactNumbers, attributes)

    getFormView: ->
      new ContactNumber.Views.FormView
        model: @contactNumbers

    getTableView: ->
      new ContactNumber.Views.TableView
        model: @contactNumbers

    # close: (args...) ->
    #   console.log "deleting region .... ", args
    #   delete @region
    #   super args

    showForm: ->
      @currentView?.close()
      @currentView = @getFormView()
      @show @currentView

    showTable: ->
      @currentView?.close()
      @currentView = @getTableView()
      @show @currentView

    show: (view) ->
      @listenTo view, "close", @close
      @region.show view

    showView: ->
      if @hasContactNumbers()
        @showTable()
      else
        @showForm()

    hasContactNumbers: ->
      !!@contactNumbers?.get('fax_number') || @contactNumbers?.get('telephone_number')

    saveMessage: ->
      "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'>&times;</button>Saved Outlet's Contact Numbers!</div>"

    errorMessage: ->
      "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button>Error while saving Outlet's Contact Numbers!</div>"

    saveContactNumbers: (contactNumbers, attributes) ->
      savingContactNumbers = contactNumbers.save(attributes)
      savingContactNumbers.done (contactInfo) =>
        App.contactNumber = contactNumbers
        @showTable()
        $('#alert-space').html @saveMessage()
      savingContactNumbers.fail (jqXHR) =>
        console.warn "Error saving contact numbers: ", jqXHR
        @errorMessage()

  ContactNumber.on "start", ->
    @controller = new ContactNumber.Controller
      region:         App.mainRegion
      contactNumbers: App.contactNumbers
