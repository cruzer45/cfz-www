(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['contact_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n  <fieldset>\n     <legend>\n        Contact Numbers\n     </legend>\n     <div class=\"control-group\">\n        <label class=\"control-label\" for=\"telephone_number\">\n           Telephone #\n        </label>\n        <div class=\"controls\">\n           <input type=\"text\" name=\"telephone_number\" id=\"telephone_number\"\n              required value = ";
  if (stack1 = helpers.telephone_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.telephone_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + " >\n           </input>\n        </div>\n     </div>\n     <div class=\"control-group\">\n        <label class=\"control-label\" for=\"fax_number\">\n           Fax # \n        </label>\n        <div class=\"controls\">\n           <input type=\"text\" name=\"fax_number\" id=\"fax_number\"\n              value = ";
  if (stack1 = helpers.fax_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.fax_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "  >\n           </input>\n        </div>\n     </div>\n     <div class=\"control-group\">\n        <div class=\"controls\">\n           <button class=\"btn btn-primary\" id=\"save-contact-numbers-btn\">\n              <i class=\"icon-ok\"></i>\n              Save\n           </button>\n            <button class=\"btn btn-danger\" id=\"return-btn\">\n              <i class=\"icon-signout\"></i>\n               Cancel\n            </button>\n          </div>\n     </div>\n  </fieldset>\n</form> ";
  return buffer;
  });
})();