@Outlet.module "ContactNumber.Views", (Views, App, Backbone, Marionette, $, _)->

	class Views.FormView extends Marionette.ItemView
		template: Handlebars.templates['contact_form']

		events:
			'click #save-contact-numbers-btn': 'saveContactNumbers'
			'click #return-btn': 'returnBtn'

		saveContactNumbers: (e)->
			e.preventDefault()
			attributes = {telephone_number: $('#telephone_number').val(), fax_number: $('#fax_number').val()}
			App.execute "outlet:contactNumbers:save", @model, attributes
			
		returnBtn: (domEvent) ->
			domEvent.preventDefault()
			App.vent.trigger "outlet:contactNumbers:form:cancel", @model


	class Views.TableView extends Marionette.ItemView
		template: Handlebars.templates['contact_table']

		events:
			'click #edit-btn': 'showForm'

		showForm: (domEvent) ->
			domEvent.preventDefault()
			App.vent.trigger "outlet:contactNumbers:edit:form", @model
