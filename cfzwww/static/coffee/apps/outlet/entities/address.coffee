@Outlet.module "OutletEntities", (OutletEntities, App, Backbone, Marionette, $, _) ->

	class OutletEntities.OutletAddress extends Backbone.Model
		url: ->
			"/outlet/#{@get('outlet_id')}/address"

	API = 
		fetch: (outletId) ->
			API.address = new OutletEntities.OutletAddress()
			API.address.set('outlet_id', outletId)
			API.address.fetch().done(->
				App.vent.trigger "outletAddress:initialized"
			)

		get: ->
			return API.address

	App.reqres.addHandler "outlet:address:entity", ->
		API.get()

	App.commands.addHandler "fetchAddress", (outletId)->
		API.fetch(outletId)
