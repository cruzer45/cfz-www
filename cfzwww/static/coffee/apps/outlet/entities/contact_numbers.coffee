@Outlet.module "OutletEntities", (OutletEntities, App, Backbone, Marionette, $, _) ->

	class OutletEntities.ContactNumbers extends Backbone.Model
		url: ->
			"/outlet/#{@get('id')}/contact_numbers"

		defaults:
			telephone_number: ''
			fax_number: ''

	API = 
		fetch: (outletId) ->
			API.contact_numbers = new OutletEntities.ContactNumbers()
			API.contact_numbers.set('id', outletId)
			API.contact_numbers.fetch().done(->
				App.vent.trigger "contactNumbers:initialized"
			)

		get: ->
			return API.contact_numbers

	App.reqres.addHandler "outlet:contactNumbers:entity", ->
		API.get()

	App.commands.addHandler "fetchContactNumbers", (outletId)->
		API.fetch(outletId)
