@Outlet.module "OutletEntities", (OutletEntities, App, Backbone, Marionette, $, _) ->
	class OutletEntities.Manager extends Backbone.Model
		url: ->
			"/outlet/#{@get('outlet_id')}/manager"

	API = 
		fetch: (outletId) ->
			API.manager = new OutletEntities.Manager()
			API.manager.set('outlet_id', outletId)
			API.manager.fetch().done(->
				App.vent.trigger "outletManager:initialized"
			)

		get: ->
			return API.manager

	App.reqres.addHandler "outlet:manager:entity", ->
		API.get()

	App.commands.addHandler "fetchManager", (outletId)->
		API.fetch(outletId)
