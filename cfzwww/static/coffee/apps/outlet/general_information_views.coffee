@Outlet.module "GeneralInformation", (GeneralInformation, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	class GeneralInformation.BasicView extends Marionette.ItemView
		template: '#general-information-tmpl'

		onShow: ->
			$('#status').editable
				value: @model.get 'status'
				source: [
					{value: 'Active', text: 'Active'},
					{value: 'Inactive', text: 'Inactive'}
				]

			$('#status').on('save', (e, params)=>
				@model.set('status', params.newValue)
				@model.save()
			)

