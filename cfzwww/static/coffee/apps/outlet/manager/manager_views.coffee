@Outlet.module "Manager.Views", (Views, App, Backbone, Marionette, $, _)->

  class Views.Form extends Marionette.ItemView
    template: Handlebars.templates['manager_form']

    initialize: ->
      @validationErrors = []

    events:
      'click #save-btn': 'saveManager'
      'click #return-btn': 'returnBtn'
      'keyup #first-name': 'requiredFieldAlert'
      'keyup #last-name': 'requiredFieldAlert'

    onShow: ->
      $('#first-name-alert').hide()
      $('#last-name-alert').hide()


    saveManager: (e)->
      e.preventDefault()
      first_name = $('#first-name').val()
      last_name = $('#last-name').val()
      phone1 = $('#manager-phone1').val()
      phone2 = $('#manager-phone2').val()
      attributes = 
        firstName: first_name
        lastName: last_name
        phone1: phone1
        phone2: phone2
      App.execute "outlet:manager:save", @model, attributes

    returnBtn: (domEvent) ->
      domEvent.preventDefault()
      App.vent.trigger "outlet:manager:form:cancel"


    requiredFieldAlert: (e)->
      if(_.isEmpty($(e.target).val()))
        $("##{e.target.id}-alert").show()
        if e.target.id not in @validationErrors 
          @validationErrors.push(e.target.id)
      else
        $("##{e.target.id}-alert").hide()
        indexOfId = @validationErrors.indexOf(e.target.id)
        @validationErrors.splice(indexOfId, 1)
      if(@validationErrors.length > 0)
        $('#save-btn').attr("disabled", "disabled")
      else
        $('#save-btn').removeAttr("disabled")


  class Views.Table extends Marionette.ItemView
    template: Handlebars.templates['manager_table']

    events:
      'click #edit-btn': 'showForm'

    showForm: (domEvent) ->
      domEvent.preventDefault()
      App.vent.trigger "outlet:manager:edit:form", @model
