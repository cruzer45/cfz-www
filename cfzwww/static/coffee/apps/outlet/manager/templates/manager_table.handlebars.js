(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['manager_table'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<h4>Outlet Manager</h4>\n<div id=\"alert-space\">\n</div>\n\n<table class=\"display table table-striped table-bordered\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"First Name\">First Name</th>\n         <th role=\"Last Name\">Last Name</th>\n         <th role=\"Phone 1\">Phone 1</th>\n         <th role=\"Phone 2\">Phone 2</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody>\n      <td>";
  if (stack1 = helpers.first_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.first_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>";
  if (stack1 = helpers.last_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.last_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>";
  if (stack1 = helpers.phone1) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone1; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>";
  if (stack1 = helpers.phone2) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.phone2; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n      <td>\n         <button class=\"btn btn-primary\" id=\"edit-btn\" title=\"Edit Outlet Manager\">\n            Edit\n         </button>\n      </td>\n   </tbody>\n</table>\n\n";
  return buffer;
  });
})();