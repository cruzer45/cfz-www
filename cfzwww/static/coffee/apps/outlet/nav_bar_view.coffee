@Outlet.module "Navbar", (Navbar, App, Backbone, Marionette, $, _)->
	@startWithParent = false

	class Navbar.OutletNavbar extends Marionette.ItemView
		template: '#outlet-nav-tmpl'
		tagName:  'ul'
		className: 'nav nav-tabs tabs-two'

		onShow: ->
			@setAsActive("#generalInfo-tab-item")

		events:
			'click #generalInfo-tab': 'showGeneralInfo'
			'click #contactNumbers-tab': 'showContactNumbers'
			'click #address-tab': 'showAddress'
			'click #manager-tab': 'showManager'

		showGeneralInfo: ->
			App.vent.trigger "generalInformation:show"
			@setAsActive('#generalInfo-tab-item')

		showContactNumbers: ->
			App.vent.trigger "contactNumbers:request:show"
			@setAsActive('#contactNumbers-tab-item')

		showAddress: ->
			App.vent.trigger "address:request:show"
			@setAsActive('#address-tab-item')

		showManager: ->
			App.vent.trigger "manager:request:show"
			@setAsActive('#manager-tab-item')

			
		setAsActive: (id) ->
			listItems = [
				'#generalInfo-tab-item',
				'#contactNumbers-tab-item',
				'#address-tab-item',
				'#manager-tab-item'
			]
			for item in listItems
				if id == item
					$(item).toggleClass('active', true)
				else
					$(item).toggleClass('active', false)

	API =
		show: ->
			App.navBarRegion.show(new Navbar.OutletNavbar())

	Navbar.on "start", ->
		API.show()

