((app, context) ->

		class context.CompanyAppRouter extends Backbone.Marionette.AppRouter
			appRoutes: 
				"generalInformation/show": "renderGeneralInformation"
				"contactNumbers/show": "showContactNumbers"
				"outlets": "showOutlets"
				

)(app, app.module('companyListModule'))
