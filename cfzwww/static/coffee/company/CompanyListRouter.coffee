((app, context) ->

		context.routerController = 
			fetchCompanies: ->
				console.log "fetching companies"

		class context.CompanyListRouter extends Backbone.Marionette.AppRouter
			controller: context.routerController

			appRoutes: 
				"companies/list": "fetchCompanies"
				

)(app, app.module('companyListModule'))
