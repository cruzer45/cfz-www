((app, context) ->
	class context.AnnualReport extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/annual_report"


	class context.AnnualReports extends Backbone.Collection
		url: ->
			"/company/#{@company.id}/annual_reports"

		initialize: (company) ->
			@company = company

)(app, app.module('companyListModule'))
