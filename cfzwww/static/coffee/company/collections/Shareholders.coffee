((app, context) ->

	class context.Shareholder extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/shareholder"


	class context.Shareholders extends Backbone.Collection
		model: context.Shareholder

		url: ->
			"/company/#{@company.id}/shareholders"

		initialize: (company)->
			@company = company
			
)(app, app.module('companyListModule'))
