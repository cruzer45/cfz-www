((app, context) ->
	class context.VehicleSticker extends Backbone.Model
		url: ->
			"/company/#{@get('companyId')}/sticker"

		defaults:
			#date_issued: moment(new Date()).format("DD/MM/YYYY")
			owner: ''
			color: ''
			vehicle_year: ''
			vehicle_model: ''
			license_plate: ''
			month_expired: ''
			receipt_number: ''
			issued_by: ''
			sticker_number: ''



	class context.VehicleStickers extends Backbone.Collection
		url: ->
			"/company/#{@company.id}/stickers"

		initialize: (company)->
			@company = company

)(app, app.module('companyListModule'))
