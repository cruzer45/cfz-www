((app, context)->
	class context.CompanyFormModel extends Backbone.Model
		urlRoot: '/company/create'

		defaults:
			incorporation_date: moment(new Date()).format("DD/MM/YYYY")

)(app, app.module('companyBuildModule'))

