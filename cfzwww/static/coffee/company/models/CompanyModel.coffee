((app, context) ->

		class context.Company extends Backbone.Model
			urlRoot: '/company'

		class context.Address extends Backbone.Model
			url:  ->
				"/company/#{@attributes.id}/address"


		class context.ContactNumbers extends Backbone.Model
			url: ->
				"/company/#{@attributes.id}/contact_numbers"


		class context.Shareholder extends Backbone.Model
			url: ->
				'/companies/shareholder'


		class context.AnnualReport extends Backbone.Model
			url: ->
				'/companies/annualreport'

		class context.AcceptanceLetter extends Backbone.Model
			url: ->
				"/company/#{@get('id')}/letter_of_acceptance"

)(app, app.module('companyListModule'))