((app, context)->

	class context.AcceptanceLetterDate extends Backbone.Marionette.ItemView
		template: Handlebars.templates['acceptance_date']
		tagName: 'p'


	class context.AcceptanceLetterDateForm extends Backbone.Marionette.ItemView
		template: '#date-acceptance-letter-form-tmpl'

		onRender: ->
			_.defer(->
				$( "#acceptance-date-letter" ).datepicker
					showWeek: true
					firstDay: 1
					changeMonth: true
					changeYear: true
					dateFormat: "dd/mm/yyyy"
			) 

		events: 
			'click #save-acceptance-date-btn': 'saveAcceptanceDate'

		saveAcceptanceDate: (e)->
			e.preventDefault()
			console.log "saving acceptanceDate"
			@model.save({date_sent_letter_of_acceptance: $('#acceptance-date-letter').val()},{
				success: (model, response, options)->
					console.log "saved"
					bootbox.alert("Acceptance Letter Saved!", ->
						app.vent.trigger "company:acceptance_letter")
				error: ->
					console.log "error"
					bootbox.alert("Acceptance Letter Saved!")
				})


	class context.GeneralInformationView extends Backbone.Marionette.Layout
		className: 'clearfix alert-block span6'
		template: Handlebars.templates['general_information']
		
		regions:
			letterForm: '#date-acceptance-letter-form'
			acceptanceDate: '#acceptance-date'

		onRender: ->
			if @model.get('date_sent_letter_of_acceptance')
				@letterForm.show(new context.AcceptanceLetterDate({model: @model}))
			else
				acceptanceLetter  = new context.AcceptanceLetter()
				acceptanceLetter.set('id', @model.get('id'))
				@letterForm.show(new context.AcceptanceLetterDateForm({model: acceptanceLetter}))

			$('#generalInfo-tab-item').toggleClass('active', true)

		initialize: ->
			app.vent.on "company:acceptance_letter", @showAcceptanceDate, @

		showAcceptanceDate: ->
			@letterForm.show(new context.AcceptanceLetterDate({model: @model}))


	class context.ContactNumbersView extends Backbone.Marionette.ItemView
		template: '#contact-numbers-tmpl'

		events:
			'click #save-contact-numbers-btn': 'saveContactNumbers'

		saveContactNumbers: (e)->
			e.preventDefault()
			@model.save({telephone_number: $('#telephone_number').val(), fax_number: $('#fax_number').val(), cable_number: $('#cable_number').val()}, {
				success: (model, response, options)->
					alert("Contact Numbers saved!")
				error: (model, xhr, options)->
					alert('Could not save Address!')
				})


	class context.AddressView extends Backbone.Marionette.ItemView
		template: '#address-tmpl'

		events:
			'click #save-address-btn': 'saveAddress'

		saveAddress: (e)->
			e.preventDefault()
			@model.save({state: $('#state').val(), country: $('#country').val()},{
				success: (model, response, options)->
					alert("Address Saved!")
				error: (model, xhr, options)->
					alert("Could not save Address!")
			})

	class context.CompanyInformationNav extends Backbone.Marionette.ItemView
		#template: '#company-nav-tmpl'
		template: Handlebars.templates['company_nav']
		tagName:  'ul'
		className: 'nav nav-tabs tabs-two'

		events:
			'click #generalInfo-tab': 'showGeneralInfo'
			'click #contactNumbers-tab': 'showContactNumbers'
			'click #address-tab': 'showAddress'
			'click #manager-tab': 'showManager'
			'click #shareholders-tab': 'showShareholders'
			'click #annualreports-tab': 'showAnnualReports'
			'click #stickers-tab': 'showVehicleStickers'
			'click #outlets-tab': 'showOutlets'

		showGeneralInfo: ->
			app.vent.trigger "company:show:generalInformation"
			@setAsActive('#generalInfo-tab-item')

		showContactNumbers: ->
			app.vent.trigger "company:show:contactNumbers"
			@setAsActive('#contactNumbers-tab-item')

		showAddress: ->
			app.vent.trigger "company:show:address"
			@setAsActive('#address-tab-item')

		showManager: ->
			app.vent.trigger "company:show:manager"
			@setAsActive('#manager-tab-item')

		showShareholders: ->
			app.vent.trigger "company:show:shareholders"
			@setAsActive('#shareholders-tab-item')

		showAnnualReports: ->
			app.vent.trigger "company:show:annualreports"
			@setAsActive('#annualreports-tab-item')

		showVehicleStickers: ->
			app.vent.trigger "company:show:stickers"
			@setAsActive('#stickers-tab-item')

		showOutlets: ->
			app.vent.trigger "company:show:outlets"
			@setAsActive('#outlets-tab-item')
			
		setAsActive: (id) ->
			listItems = [
				'#generalInfo-tab-item',
				'#contactNumbers-tab-item',
				'#address-tab-item',
				'#manager-tab-item',
				'#shareholders-tab-item',
				'#annualreports-tab-item'
				'#stickers-tab-item'
				'#outlets-tab-item'
			]
			for item in listItems
				if id == item
					$(item).toggleClass('active', true)
				else
					$(item).toggleClass('active', false)



)(app, app.module('companyListModule'))