((app, context)->

	class context.ShareholderView extends Backbone.Marionette.ItemView
		template: '#shareholders-tmpl'
		tagName: 'tr'

		events:
			'click #edit-shareholder-btn': 'editShareholder'

		editShareholder: ->
			app.vent.trigger 'shareholder:edit:showForm', @model

	class context.ShareholderCollectionView extends Backbone.Marionette.CompositeView
		itemView: context.ShareholderView
		itemViewContainer: 'tbody'
		template: '#shareholders-table-tmpl'

		events:
			'click #add-shareholder-btn': 'showForm'

		showForm: ->
			app.vent.trigger 'shareholder:new:form'


	class context.ShareholderFormView extends Backbone.Marionette.ItemView
		template: '#shareholder-form-tmpl'

		events:
			'click #cancel-shareholder-btn': 'cancelForm'
			'blur #shareholder-first-name': 'requiredFieldAlert'
			'blur #shareholder-last-name': 'requiredFieldAlert'
			'blur #shares': 'requiredFieldAlert'
			'click #save-shareholder-btn': 'saveShareholder'

		onRender: ->
			_.defer(->
				$('#shareholder-first-name-alert').hide()
				$('#shareholder-last-name-alert').hide()
				$('#shares-alert').hide()
				$('#save-shareholder-btn').attr('disabled', 'disabled')
			)

		initialize: ->
			@validationErrors = ['shareholder-first-name', 'shareholder-last-name', 'shares']

		cancelForm: (e)->
			e.preventDefault()
			app.vent.trigger 'shareholder:cancelForm'

		requiredFieldAlert: (e)->
			if(_.isEmpty($(e.target).val()))
				$("##{e.target.id}-alert").show()
				if e.target.id not in @validationErrors 
					@validationErrors.push(e.target.id)
			else
				$("##{e.target.id}-alert").hide()
				indexOfId = @validationErrors.indexOf(e.target.id)
				@validationErrors.splice(indexOfId, 1)
			if(@validationErrors.length > 0)
				$('#save-shareholder-btn').attr("disabled", "disabled")
			else
				$('#save-shareholder-btn').removeAttr("disabled")

		saveShareholder: (e)->
			e.preventDefault()
			first_name = $('#shareholder-first-name').val()
			last_name = $('#shareholder-last-name').val()
			shares = $('#shares').val()
			@model.save({
				first_name: first_name,
				last_name: last_name,
				shares: shares},
				{success: (model, response, options)->
					bootbox.alert('Shareholder Saved!', ->
						app.vent.trigger "shareholder:save:success"
					)

				})


)(app, app.module('companyListModule'))
