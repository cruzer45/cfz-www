(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['authorizedpersons_form'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"well form-horizontal\">\n   <fieldset>\n      <legend>Authorized Person</legend>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"first_name\">First Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"first_name\" id=\"first-name\"\n               value = \"";
  if (stack1 = helpers.first_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.first_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" >\n            </input>\n            <span class=\"alert alert-error\" \n               id=\"first-name-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"last_name\">Last Name</label>\n         <div class=\"controls\">\n            <input type=\"text\" name=\"last_name\" id=\"last-name\"\n               value=\"";
  if (stack1 = helpers.last_name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.last_name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n            </input>\n            <span class=\"alert alert-error\" id=\"last-name-alert\">\n               Please provide a valid value!\n            </span>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <label class=\"control-label\" for=\"representation_type\">Representation Type</label>\n         <div class=\"controls\">\n            <select name=\"representation_type\" id=\"representation-type\">\n            </select>\n         </div>\n      </div>\n      <div class=\"control-group\">\n         <div class=\"controls\">\n            <button class=\"btn btn-primary\" id=\"save-authorizedperson-btn\">\n               <i class=\"icon-ok\"></i>\n               Save\n            </button>\n            <button class=\"btn btn-danger\" id=\"cancel-authorizedperson-btn\">\n               <i class=\"icon-signout\"></i>\n               Cancel\n            </button>\n         </div>\n      </div>\n   </fieldset>\n</form>\n\n";
  return buffer;
  });
})();