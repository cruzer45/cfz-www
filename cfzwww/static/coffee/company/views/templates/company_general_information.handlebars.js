(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['company_general_information'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"well span6\">\n      <legend><h1>";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</h1></legend>\n      <p>Registration Number: ";
  if (stack1 = helpers.registration_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.registration_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n      <p>Incorporation Date: ";
  if (stack1 = helpers.pretty_incorporation_date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.pretty_incorporation_date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n      <p>Telephone Number: ";
  if (stack1 = helpers.telephone_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.telephone_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n      <p>Date Created: ";
  if (stack1 = helpers.created_date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.created_date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n      <div id=\"acceptance-date\">\n      </div>\n      <div id=\"date-acceptance-letter-form\">\n      </div>\n      <div>\n         <p>Status: <a href=\"#\" id=\"status\" data-type=\"select\" data-original-title=\"Status\"></a></p>\n      </div>\n</div>\n<div class=\"span6\" id=\"authorized-persons-region\">\n</div>\n<div class=\"span6\" id=\"opcontracts-region\">\n</div>\n\n";
  return buffer;
  });
})();