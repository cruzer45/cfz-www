(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['courtesypass_list'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<td>";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>";
  if (stack1 = helpers.person) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.person; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>";
  if (stack1 = helpers.pretty_date_issued) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.pretty_date_issued; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>";
  if (stack1 = helpers.month_expired) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.month_expired; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>";
  if (stack1 = helpers.vehicle_model) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.vehicle_model; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>";
  if (stack1 = helpers.license_plate) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.license_plate; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>";
  if (stack1 = helpers.sticker_number) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.sticker_number; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n<td>\n   <button class=\"btn btn-primary\" id=\"edit-courtesypass-btn\" title=\"View\">\n      <i class=\"icon-eye-open\"></i> \n   </button>\n</td>\n\n";
  return buffer;
  });
})();