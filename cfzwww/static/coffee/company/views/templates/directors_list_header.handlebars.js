(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['directors_list_header'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"span6\">\n   <div class=\"button-action\">\n   <button class=\"btn btn-small btn-af\" id=\"add-director-btn\">\n      <span>\n         <i class=\"icon-small icon-list-alt\"></i>\n      </span>\n      Add Director\n   </button>\n   </div>\n</div>\n<div id=\"director-form\">\n</div>\n<table class=\"display table table-striped table-bordered\" aria-describedby=\"list-of-directors\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"columnheader\">Id</th>\n         <th role=\"First Name\">First Name</th>\n         <th role=\"Last Name\">Last Name</th>\n         <th role=\"Email\">Email</th>\n         <th role=\"Phone\">Phone #</th>\n         <th role=\"Start Date\">Start Date</th>\n         <th role=\"End Date\">End Date</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody></tbody>\n</table>\n\n";
  });
})();