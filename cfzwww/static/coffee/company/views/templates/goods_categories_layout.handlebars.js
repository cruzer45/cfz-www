(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['goods_categories_layout'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div id=\"goods-form-region\">\n   <form class=\"form-inline\">\n      <label>Select Type of Goods</label>\n         <span id=\"goods-select-region\">\n         </span>\n      <button id=\"goods-categories-btn\" type=\"submit\" class=\"btn\">Add</button>\n   </form>\n</div>\n<div id=\"goods-list-region\">\n</div>\n";
  });
})();