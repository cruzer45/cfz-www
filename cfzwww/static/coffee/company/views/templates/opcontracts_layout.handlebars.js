(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['opcontracts_layout'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"well\">\n   <legend><h3 style=\"clear: left\">Operation Contracts</h3></legend>\n   <div>\n      <span id=\"create-opcontract-btn\" class=\"span6\">\n      </span>\n   </div>\n   <div id=\"opcontracts-list\">\n   </div>\n</div>\n";
  });
})();