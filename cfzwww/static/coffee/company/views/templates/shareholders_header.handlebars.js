(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['shareholders_header'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"span6\">\n   <div class=\"button-action\">\n   <button class=\"btn btn-small btn-af\" id=\"add-shareholder-btn\">\n      <span>\n         <i class=\"icon-small icon-list-alt\"></i>\n      </span>\n      Add Shareholder\n   </button>\n   </div>\n</div>\n<div id=\"shareholder-form\">\n</div>\n<table class=\"display table table-striped table-bordered\" aria-describedby=\"list-of-shareholders\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"columnheader\">Id</th>\n         <th role=\"First Name\">First Name</th>\n         <th role=\"Last Name\">Last Name</th>\n         <th role=\"Shares\">Shares</th>\n         <th role=\"Background\">Background Check</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody></tbody>\n</table>\n\n";
  });
})();