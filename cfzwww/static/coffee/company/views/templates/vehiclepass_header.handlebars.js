(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['vehiclepass_header'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<table class=\"display table table-striped table-bordered\" aria-describedby=\"list-of-reports\">\n   <thead>\n      <tr role=\"row\">\n         <th role=\"columnheader\">Id</th>\n         <th role=\"Owner\">Owner</th>\n         <th role=\"Date Issued\">Date Issued</th>\n         <th role=\"Month Expired\">Month Expired</th>\n         <th role=\"Vehicle Model\">Vehicle Model</th>\n         <th role=\"License Plate\">License Plate</th>\n         <th role=\"Sticker #\">Sticker #</th>\n         <th/>\n      </tr>\n   </thead>\n   <tbody></tbody>\n</table>\n\n";
  });
})();