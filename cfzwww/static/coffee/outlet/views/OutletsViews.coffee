((app, context)->

	class context.OutletView extends Backbone.Marionette.ItemView
		template: '#outlet-tmpl'
		tagName: 'tr'


	class context.OutletsCollectionView extends Backbone.Marionette.CompositeView
		template: '#outlets-table-tmpl'
		itemView: context.OutletView
		itemViewContainer: 'tbody'

	class context.OutletLayout extends Backbone.Marionette.Layout
		template: '#outlet-layout-tmpl'

		regions:
			btnRegion: '#outlet-create-region'
			listRegion: '#outlet-list-region'

	class context.CreateOutletView extends Backbone.Marionette.ItemView
		template: '#outlet-create-btns'

		initialize: ->
			@model.set('createUrl', @createUrl())
			console.log "model.attributes: ", @model.attributes
			console.log "createUrl: ", @createUrl()

		createUrl: ->
			"/company/#{@model.id}/outlet/create"


)(app, app.module('companyListModule'))
