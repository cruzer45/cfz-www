define(
   ['jquery', 'lodash', 'backbone', 'router-vehiclepass'],
   function($, _, backbone, Router){
      var initialize = function(){
         Router.initialize();
      };

      return {initialize: initialize};
});

