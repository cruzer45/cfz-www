var CFZ = CFZ || {};
CFZ.Url = {};
CFZ.Url.Path = {
  root: "http://cfz.stumblingoncode.com:8080/" ,
  for: function(path){
               return this.root + path;
            }
}

CFZ.is_present = function(item){
   var present = true;
   if(item == undefined){
      present = false;
   }
   return present;
}

CFZ.is_numeric = function(item){
   var numeric = false;
   if(!isNaN(item)){
      numeric = true;
   }
   return numeric;
}
