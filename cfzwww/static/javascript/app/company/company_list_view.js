define([
       'jquery',
       'lodash',
       'backbone',
       'app/company/companies_collections',
       'text!templates/company/list.html'
], function($, _, backbone, companiesCollection, companyListTemplate){
   var companyListView = Backbone.View.extend({
      el: $("#companies"),
      initialize: function(){
         this.collection = companiesCollection;
      },
      render: function(){
         this.loadResults();
      },

      loadResults: function(){
         this.collection.fetch({success: function(collections){
            var data = {
               companies: collections.models,
               _: _
            };
            var compiledTemplate = _.template(companyListTemplate, data);
            $("#companies").html(compiledTemplate);
         }});
      }
   });

   return new companyListView();
});
