define([
       'jquery',
       'lodash',
       'backbone',
       'app/company/companies_collections',
       'text!templates/company/selectbox.html'
], function($, _, backbone, companiesCollection, companyListTemplate){
   var companySelectboxView = Backbone.View.extend({
      el: $("#companies-selectbox"),
      initialize: function(){
         this.collection = companiesCollection;
      },
      render: function(){
         this.loadResults();
      },

      loadResults: function(){
         this.collection.fetch({success: function(collections){
            var data = {
               companies: collections.models,
               _: _
            };
            var compiledTemplate = _.template(companyListTemplate, data);
            $("#companies-selectbox").html(compiledTemplate);
         }});
      }
   });

   return new companySelectboxView();
});

