var editAppModule = angular.module("editAppModule", ["ngResource"]);

editAppModule.service('applicationDataService', function($rootScope, $http){

   this.application_data = {};
   this.getApplication = function(id){
         $http({method: 'GET', url:CFZ.Url.Path.for('application/get/'+id)}).
            success(function(data,status){
               this.application_data = angular.copy(data);
               $rootScope.$broadcast('applicationDataRetrieved');
            });
   }

});

function MainController($scope, $http, $location, applicationDataService){
   var path = window.location.pathname.split("/");
   var id = path[path.length - 1];
   applicationDataService.getApplication(id);
}


function EditController($scope, $http, $location, applicationDataService){

   var path = window.location.pathname.split("/");
   var id = path[path.length - 1];
   console.log("path: " + path);
   console.log("id: " + id);
   $scope.application_map = {};

   $scope.$on('applicationDataRetrieved', function(){
      $scope.application= angular.copy(application_data);
      console.log("application: " + JSON.stringify($scope.application));
   });
}
