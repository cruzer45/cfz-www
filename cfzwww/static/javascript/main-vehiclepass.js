require.config({
   paths: {
      bootstrap:  'bootstrap.min',
      jquery: 'libs/jquery-1.7.2.min',
      jqueryui: 'libs/jquery-ui-1.8.21.custom.min',
      lodash: 'libs/lodash.min',
      backbone: 'libs/backbone.min',
      underscore: 'libs/underscore',
      vehiclePassModule: 'app/vehicle_pass/vehicle_pass_module',
      templates: '/static/templates'
   },
   shim: {
      'bootstrap': ['jquery'],
      'jqueryui': ['jquery'],
      'backbone': ['lodash', 'jquery'],
      'vehiclePassModule': ['lodash', 'backbone']
   }
});

require(['app-vehiclepass'], function(App){
   App.initialize();
});
