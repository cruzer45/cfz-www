define(
   [
      'jquery',
      'lodash',
      'backbone',
      'app/company/company_list_view',
      'app/vehicle_pass/vehiclepass_create_view'
], function($,_, backbone, companyListView, vehiclePassCreateView){
      var AppRouter = Backbone.Router.extend({
         routes: {
            "vehicle_pass/list": "vehiclePassListView",
            "company/list": "showCompanyListView",
            "vehiclepass/create": "showVehiclePassForm",
            //Defaults
            "*actions": "defaultAction"
         },

         showVehiclePassForm: function(){
            vehiclePassCreateView.render();
         },

         vehiclePassListView: function(){
            alert("show lists of vehicle pass")
         },

         showCompanyListView: function(){
            companyListView.render();
         },
         
         defaultAction: function(actions){
            alert("Default");
         }

      });

      var initialize = function(){
         console.log("Initialized Routes....");
         var app_router = new AppRouter();
         Backbone.history.start();
      };

      return{
         initialize: initialize
      };
});

