from pyramid.response import Response
from pyramid.view import view_config, forbidden_view_config
from pyramid.url import route_url
from pyramid.httpexceptions import (
    HTTPFound,
    HTTPForbidden)
from pyramid.security import (
    remember,
    forget,
    effective_principals,
    Authenticated,
    authenticated_userid)
from sqlalchemy.exc import DBAPIError
from ..models import (
    DBSession,
    MyModel)
from cfzpersistence.repository.auth import UserAccountRepository
from ..services import auth


@view_config(route_name="test", renderer="templates/test.jinja2")
def test(request):
    return dict(request=request)


@view_config(route_name="index",
             permission=Authenticated,
             renderer="templates/index.jinja2")
def index(request):
    user_id = authenticated_userid(request)
    user = auth.find_user_by_id(user_id)
    return dict(page="index", username=user.username)


@view_config(route_name="login", renderer="templates/login.jinja2")
def login(request):
    return dict(request=request, page="login")


@view_config(route_name="doLogin")
def do_login(request):
    main_view = route_url('index', request)
    login_view = route_url('login', request)
    came_from = request.params.get('came_from', main_view)

    post_data = request.POST
    if 'submit' in post_data:
        login = post_data['username']
        password = post_data['password']

        #Extract user
        user_repository = UserAccountRepository(DBSession)
        user = user_repository.find(login)
        if user and user.check_password(password):
            headers = remember(request, user.id)
            request.session.flash(u'Signed in Successfully!')
            request.session['username'] = user.username
            return HTTPFound(location=came_from, headers=headers)

    login_failed_message = u'<div class="alert alert-error">\
        Failed To Login!</div>'
    request.session.flash(login_failed_message)
    return HTTPFound(location=login_view)


@view_config(route_name="logout")
def logout(request):
    request.session.invalidate()
    logout_message = u'<div class="alert alert-info">Signed Out!</div>'
    request.session.flash(logout_message)
    headers = forget(request)
    return HTTPFound(location=route_url('login', request), headers=headers)


@forbidden_view_config()
def forbidden_view(request):
    # do not allow a user to login if they are already logged in
    if authenticated_userid(request):
        return HTTPForbidden()

    loc = request.route_url('login', _query=(('next', request.path),))
    return HTTPFound(location=loc)


def get_user(request):
    from pyramid.security import authenticated_userid
    from ..models import user_repository
    user_id = authenticated_userid(request)
    return user_repository.get(user_id)
