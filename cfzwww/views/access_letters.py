from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.url import route_url
from ..models import access_letter_repository
from cfzcore.letters import (
    AccessLetter,
    AccessLetterType)


@view_config(route_name="access_letter_main",
             permission="admin",
             renderer="templates/kuta/access_letters/main.jinja2")
def main(request):
    return dict(page="access_letters")


@view_config(route_name="access_letter_list",
             permission="admin",
             renderer="templates/kuta/access_letters/list.jinja2")
def list(request):
    letter_type = getattr(AccessLetterType, request.matchdict['letter_type'])
    letters = access_letter_repository.list_by_letter_type(letter_type)
    total = access_letter_repository.count_by_letter_type(letter_type)
    return dict(
        page="access_letters",
        access_letter_selection=request.matchdict['letter_type'],
        letter_type=request.matchdict['letter_type'],
        total=total,
        form_header=' '.join(request.matchdict['letter_type'].split('_')),
        access_letters=_access_letter_presenters(letters))


def _access_letter_presenters(letters):
    return [AccessLetterFormPresenter(letter) for letter in letters]


@view_config(
    route_name="access_letter_new",
    permission="admin",
    renderer="templates/kuta/access_letters/form.jinja2")
def show_form(request):
    access_letter = AccessLetter()
    letter_type = getattr(AccessLetterType, request.matchdict['letter_type'])
    letter_type = request.matchdict['letter_type']
    return dict(
        page="access_letters",
        access_letter_selection=letter_type,
        form_header="Create Access Letter",
        action=route_url('access_letter_create',
                    request,
                    letter_type=letter_type),
        access_letter=AccessLetterFormPresenter(access_letter))


@view_config(
    route_name="access_letter_show",
    permission="admin",
    renderer="templates/kuta/access_letters/form.jinja2")
def access_letter_show(request):
    access_letter = access_letter_repository.get(request.matchdict['id'])
    letter_type = get_letter_type_name(access_letter.letter_type)
    return dict(
        page="access_letters",
        access_letter_selection=letter_type,
        form_header="Edit Access Letter",
        action=route_url('access_letter_create',
            request,
            letter_type=letter_type),
        access_letter=AccessLetterFormPresenter(access_letter))


@view_config(
    route_name="access_letter_create",
    permission="admin")
def create(request):
    from ..services.access_letters import AccessLetterCrudService
    from ..views import get_user
    letter_type = request.matchdict['letter_type']
    success_view = route_url(
        'access_letter_list',
        request, letter_type=letter_type)
    post_data = request.POST
    if 'submit' in post_data:
        _validate_letter_form(request, **post_data)
        if request.session.peek_flash():
            fail_view = route_url(
                'access_letter_new',
                request,
                letter_type=letter_type)
            return HTTPFound(location=fail_view)
        else:
            post_data['letter_type'] = letter_type
            post_data['issued_by'] = get_user(request).username
            if post_data['id'] is not None:
                AccessLetterCrudService().save(**post_data)
            else:
                AccessLetterCrudService().create(**post_data)
            request.session.flash(
                u'<div class="alert alert-info">Access Letter saved successfully!</div>')
    return HTTPFound(location=success_view)


def _validate_letter_form(request, **kwargs):
    for field in _required_fields():
        if not kwargs.get(field['name'], None):
            request.session.flash(field['msg'])


def _required_fields():
    granted_to_msg = u'<div class="alert alert-error">You must specify who this access letter is being granted to.</div>'
    license_msg = u'<div class="alert alert-error">You must specify a vehicle license plate</div>'
    expiration_date_msg = u'<div class="alert alert-error">Access Letter requires an expiration date</div>'
    processing_fee_msg = u'<div class="alert alert-error">Access Letter requires a processing fee.</div>'
    authorized_by_msg = u'<div class="alert alert-error">Someone must authorize the access letter.</div>'
    return [
        {
            'name': 'granted_to',
            'msg': granted_to_msg
        },
        {
            'name': 'vehicle_license_plate',
            'msg': license_msg
        },
        {
            'name': 'expiration_date',
            'msg': expiration_date_msg
        },
        {
            'name': 'processing_fee',
            'msg': processing_fee_msg
        },
        {
            'name': 'authorized_by',
            'msg': authorized_by_msg
        }
    ]


def get_letter_type_name(letter_type):
    for k in AccessLetterType.__dict__:
        if getattr(AccessLetterType, k) == letter_type:
            return k


class AccessLetterFormPresenter(object):
    def __init__(self, access_letter):
        from datetime import datetime
        self.id = getattr(access_letter, 'id', 0)
        self.granted_to = access_letter.granted_to
        self.vehicle_license_plate = access_letter.vehicle_license_plate
        self.processing_fee = access_letter.processing_fee
        self.authorized_by = access_letter.authorized_by
        self.expiration_date = datetime.strftime(
            access_letter.expiration_date, '%d/%m/%Y')
        self.pretty_expiration_date = datetime.strftime(
            access_letter.expiration_date, '%d/%b/%Y')
        if getattr(access_letter, 'date_processed', None):
            self.date_processed = datetime.strftime(
                access_letter.date_processed, '%d/%b/%Y')
