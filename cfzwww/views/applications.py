from pyramid.view import view_config
from pyramid.renderers import render_to_response
from pyramid.httpexceptions import HTTPFound
from ..models import DBSession
from ..presenters import (ApplicationPresenter,
                          PersonPresenter,
                          DeniedApplicationPresenter,
                          InvestorPresenter)
from ..presenters.errors import FormIncompleteErrorPresenter

from cfzpersistence.repository.application import ApplicationRepository
from cfzcore.interactors.application_builder import (ApplicationFormBuilder,
                                                     FormIncompleteException)
from cfzcore.interactors.application import (ApplicationBasicInformation,
                                             ApplicationInvestorForm)
from cfzcore.approvers import (InvestorBackgroundCheckFailed,
                               InvestorBackgroundCheckEmptyException,
                               NoInvestorsException)

from cfzwww.services import application_approver

from collections import namedtuple


@view_config(route_name="applications",
  permission="admin",
  renderer="templates/application/list.jinja2")
def applications_list(request):
    application_repository = ApplicationRepository(DBSession)
    applications = application_repository.all_with_status_new()
    username = request.session.get('username', None)
    return dict(page="applications",
                sidebar="new_applications",
                username=username,
                applications=applications)


@view_config(route_name="new_application_form",
             renderer="templates/application/create_application_form.jinja2")
def application_create_form(request):
    company_types = ["Import/Export", "Manufacturing"]
    return dict(page="applications",
                company_types=company_types,
                sidebar="create_application")


@view_config(route_name="create_application")
def create_application(request):
    post_data = request.POST
    application_repository = ApplicationRepository(DBSession)
    builder = ApplicationFormBuilder(application_repository)
    application = builder.create_application(**post_data)
    go_to = "/applications/show/" + str(application.id)
    return HTTPFound(location=go_to)


@view_config(route_name="show_application",
             renderer="templates/application/show.jinja2")
def show_application(request):
    application_id = request.matchdict['id']
    return dict(page="applications",
                sidebar="new_applications",
                id=application_id)


@view_config(route_name="get_application", renderer="json")
def get_application(request):
    application_id = request.matchdict['id']
    application_repository = ApplicationRepository(DBSession)
    application = application_repository.get(int(application_id))
    presenter = ApplicationPresenter(application)
    return presenter


@view_config(route_name="update_general_information",
             renderer="json")
def update_general_information(request):
    try:
        application_repository = ApplicationRepository(DBSession)
        form_builder = ApplicationBasicInformation(application_repository)
        submitted_data = request.json_body
        submitted_data['application_id'] = int(request.matchdict['id'])
        form_builder.update_basic_information(**submitted_data)
        return HTTPFound("/applications/" + str(request.matchdict['id']) + '/general_information/ok')
    except FormIncompleteException, (exception):
        errors = FormIncompleteErrorPresenter(exception.error)
        return render_to_response('templates/application/general_information_error.jinja2',
                                  dict(errors=errors),
                                  request=request)


@view_config(route_name="update_general_information_ok",
             renderer="templates/application/general_information_success.jinja2")
def update_general_information_ok(request):
    return dict()


@view_config(route_name="update_application_contact_information",
             renderer="json")
def update_contact_information(request):
    submitted_data = request.json_body
    submitted_data['application_id'] = int(request.matchdict['id'])
    application_repository = ApplicationRepository(DBSession)
    basic_information_form = ApplicationBasicInformation(application_repository)
    application = basic_information_form.save_contact_numbers(**submitted_data)
    presenter = ApplicationPresenter(application)
    return presenter


@view_config(route_name="update_application_address",
             renderer="json")
def save_address(request):
    submitted_data = request.json_body
    submitted_data["application_id"] = int(request.matchdict["id"])
    application_repository = ApplicationRepository(DBSession)
    basic_information_form = ApplicationBasicInformation(application_repository)
    application = basic_information_form.save_company_location(**submitted_data)
    presenter = ApplicationPresenter(application)
    return presenter


@view_config(route_name="save_application_consultant",
             renderer="json")
def save_consultant(request):
    submitted_data = request.json_body
    application_repository = ApplicationRepository(DBSession)
    builder = ApplicationFormBuilder(application_repository)
    consultant = builder.save_consultant_for(int(request.matchdict['id']),
                                             **submitted_data)
    consultant_presenter = PersonPresenter(consultant)
    return consultant_presenter


@view_config(route_name="save_application_manager",
             renderer="json")
def save_manager(request):
    submitted_data = request.json_body
    application_repository = ApplicationRepository(DBSession)
    builder = ApplicationFormBuilder(application_repository)
    manager = builder.update_manager_for(int(request.matchdict['id']),
                                         **submitted_data)
    manager_presenter = PersonPresenter(manager)
    return manager_presenter


@view_config(route_name="save_application_description_of_goods",
             renderer="json")
def save_description_of_goods(request):
    submitted_data = request.json_body
    submitted_data["application_id"] = int(request.matchdict['id'])
    application_repository = ApplicationRepository(DBSession)
    basic_information_form = ApplicationBasicInformation(application_repository)
    basic_information_form.save_description_of_goods(**submitted_data)
    return dict()


@view_config(route_name="application_investors",
             renderer="json")
def investors(request):
    application_repository = ApplicationRepository(DBSession)
    investor_form = ApplicationInvestorForm(application_repository)
    _investors = investor_form.get_all_investors_for(int(request.matchdict['id']))
    investors = [InvestorPresenter(investor) for investor in _investors]
    return investors


@view_config(route_name="application_add_investor",
             renderer="json")
def create_investor(request):
    application_repository = ApplicationRepository(DBSession)
    investor_form = ApplicationInvestorForm(application_repository)
    submitted_data = request.json_body
    Investor = namedtuple("Investor", "first_name last_name percent_owned")
    investor = Investor(first_name=submitted_data['first_name'],
                        last_name=submitted_data['last_name'],
                        percent_owned=submitted_data['percent_owned'])
    investor_form.add_investor_to(int(submitted_data["id"]), investor)
    return HTTPFound('/applications/' + str(request.matchdict['id']) + '/investors')


@view_config(route_name="application_remove_investor",
             renderer="json")
def delete_investor(request):
    application_repository = ApplicationRepository(DBSession)
    investor_form = ApplicationInvestorForm(application_repository)
    investor_form.remove_investor_from(int(request.matchdict['application_id']),
                                       int(request.matchdict['id']))
    return dict()


@view_config(route_name="pending_applications",
             renderer="templates/application/pending_applications.jinja2")
def pending_applications(request):
    return dict(page="applications",
                sidebar="pending_applications")


@view_config(route_name="pending_applications_list",
             renderer="json")
def pending_applications_list(request):
    application_repository = ApplicationRepository(DBSession)
    applications = application_repository.all_with_status_pending()
    application_list = [ApplicationPresenter(application.__tuple__) for application in applications]
    return application_list


@view_config(route_name="approve_application",
             renderer="json")
def approve(request):
    try:
        application = application_approver.approve(int(request.matchdict['id']))
        return dict(company=application.company.name)
    except InvestorBackgroundCheckEmptyException, (exception):
        return render_to_response('templates/application/approve_error_message.jinja2',
                                  dict(investors=exception.error.investors),
                                  request=request)
    except InvestorBackgroundCheckFailed, (exception):
        return render_to_response('templates/application/investor_failed_background_check_message.jinja2',
                                  dict(shareholder=exception.error),
                                  request)
    except NoInvestorsException, (exception):
        return render_to_response('templates/application/approve_no_investors_error_message.jinja2',
                                  dict(),
                                  request)


@view_config(route_name="deny_application",
             renderer="json")
def deny(request):
    reason_for_denial = request.json_body['reason_for_denial']
    return dict(company=application_approver.deny(int(request.matchdict['id']),
                                                  reason_for_denial))


@view_config(route_name="denied_applications_list",
             renderer="templates/application/denied_list.jinja2")
def denied_applications(request):
    application_repository = ApplicationRepository(DBSession)
    denied_applications = [DeniedApplicationPresenter(application) for application in application_repository.all_denied_applications()]
    return dict(applications=denied_applications,
                page='applications',
                sidebar='denied_applications')
