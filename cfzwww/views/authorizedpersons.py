from pyramid.view import view_config

from ..services import (authorizedrep_crudservice,
                      company_crudservice,
                      representationtype_crudservice,
                     )
from ..presenters.authorizedpersons import (RepresentationTypePresenter,
                                            AuthorizedRepresentativePresenter,
                                            )


@view_config(route_name='representation_type',
             renderer='json',
             request_method='GET')
def get_representation_types(request):
    representation_types = representationtype_crudservice.all()
    return [RepresentationTypePresenter(representation) for representation in representation_types]


@view_config(route_name="authorizedrepresentative_new",
             renderer='json',
             request_method='POST')
def create_authorizedrepresentative(request):
    company = company_crudservice.get_with_id(int(request.matchdict['company_id']))
    representation_type = representationtype_crudservice.get(int(request.json_body['representation_type']))
    params = dict()
    params.update(request.json_body)
    params['company'] = company
    params['representation_type'] = representation_type
    authorizedrepresentative = authorizedrep_crudservice.create_authorizedrepresentative(**params)
    return AuthorizedRepresentativePresenter(authorizedrepresentative)


@view_config(route_name="authorizedrepresentatives_delete",
             renderer="json",
             request_method="DELETE")
def delete_authorizedrepresentative(request):
    representative_id = request.matchdict['id']
    authorizedrep_crudservice.delete(representative_id)
    return []


@view_config(route_name="authorizedrepresentative",
             renderer="json",
             request_method='PUT')
def update_authorizedrepresentative(request):
    return []


@view_config(route_name="authorizedrepresentatives_list",
             renderer="json",
             request_method="GET")
def list_authorizedrepresentatives(request):
    company = company_crudservice.get_with_id(request.matchdict['company_id'])
    authorized_representatives = authorizedrep_crudservice.list_authorizedrepresentatives(company)
    return [AuthorizedRepresentativePresenter(authrep) for authrep in authorized_representatives]

