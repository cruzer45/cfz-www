from pyramid.view import view_config

from ..models import (DBSession,
                      company_repository,
                      goodscategory_repository)

from cfzpersistence.repository.categories import GoodsCategoryRepository
from cfzpersistence.repository.company import CompanyRepository
from ..presenters.goods_categories import GoodCategoryPresenter


@view_config(route_name='goods_categories_show',
             renderer="templates/kuta/goods_categories_admin/index.jinja2")
def goods_categories_show(request):
    return dict(page="goods_categories_show")


@view_config(route_name='goods_categories', renderer='json')
def goods_categories(request):
    repository = GoodsCategoryRepository(DBSession)
    goods = repository.all()
    return [GoodCategoryPresenter(good) for good in goods]


@view_config(route_name='company_goods', renderer='json')
def company_goods(request):
    repository = CompanyRepository(DBSession)
    company = repository.get(int(request.matchdict['id']))
    goods_categories = company.goods_categories
    if goods_categories:
        return toGoodsPresenters(company.goods_categories)
    else:
        return []


def toGoodsPresenters(goods_categories):
    return [GoodCategoryPresenter(good) for good in goods_categories]


@view_config(route_name='company_good', renderer='json', request_method='PUT')
def company_good(request):
    company = company_repository.get(int(request.matchdict['id']))
    goods_category = goodscategory_repository.get(request.matchdict['category_id'])
    company.goods_categories.append(goods_category)
    company_repository.persist(company)
    return GoodCategoryPresenter(goods_category)


@view_config(route_name='company_good', renderer='json', request_method='DELETE')
def delete_company_good(request):
    company = company_repository.get(int(request.matchdict['id']))
    goods_category = goodscategory_repository.get(request.matchdict['category_id'])
    company.goods_categories.remove(goods_category)
    company_repository.persist(company)
    return []


@view_config(route_name='goods_category',
             renderer='json',
             request_method='POST')
def create_good_category(request):
    from cfzcore.goods import GoodsCategory
    goods_category = GoodsCategory(request.json_body['name'])
    goods_category = goodscategory_repository.persist(goods_category)
    return GoodCategoryPresenter(goods_category)


@view_config(route_name='goods_category_item',
             renderer='json',
             request_method='PUT')
def update_good_category(request):
    goods_category = goodscategory_repository.get(request.matchdict['id'])
    goods_category.name = request.json_body['name']
    goods_category = goodscategory_repository.persist(goods_category)
    return GoodCategoryPresenter(goods_category)


@view_config(route_name='goods_category_item',
             renderer='json',
             request_method='DELETE')
def delete_good_category(request):
    goods_category = goodscategory_repository.get(request.matchdict['id'])
    goodscategory_repository.remove(goods_category)
    return dict()

