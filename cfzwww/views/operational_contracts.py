from pyramid.view import view_config
from pyramid.response import Response
from ..models import (company_repository,
                      opcontract_repository)
from ..models import (cfz_ceo, cfz_office, processing_fee)
from ..presenters.contracts import  OperationContractPresenter
from cfzcore.company.docs.contracts import OperationalContract


@view_config(route_name="operational_contract_list",
        request_method="GET", 
        renderer="json")
def operational_contract_list(request):
    company = company_repository.get(int(request.matchdict['id']))
    operation_contracts = [OperationContractPresenter(opcontract) for opcontract in company.operational_contracts]
    return operation_contracts


@view_config(route_name="operational_contract",
        request_method="POST", 
        renderer="json")
def operational_contract(request):
    from cfzcore.interactors.company import CompanyDirectorEditor
    from datetime import datetime
    company_editor = CompanyDirectorEditor()
    company = company_repository.get(int(request.matchdict['id']))
    company_types = [company_type.name for company_type in company.company_types]
    goods_categories = [goods.name for goods in company.goods_categories]
    params = {
            'company': company,
            'cfz_ceo': cfz_ceo,
            'cfz_office': cfz_office,
            'processing_fee': processing_fee,
            'director': company_editor.get_current_director(company).person.name(),
            'company_types': (', ').join(company_types),
            'goods': (', ').join(goods_categories),
            'seal_date': datetime.now().strftime('%d/%m/%Y')
            }
    opcontract = opcontract_repository.persist(OperationalContract.create_with(**params))
    return OperationContractPresenter(opcontract)


@view_config(route_name="operational_contract_print",
        request_method="GET",
        renderer="templates/kuta/company/opcontract.jinja2")
def operational_contract_print(request):
    opcontract = opcontract_repository.get(int(request.matchdict['id']))
    return dict(opcontract=OperationContractPresenter(opcontract))
