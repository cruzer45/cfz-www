from pyramid.view import view_config
from ..models import DBSession
from cfzpersistence.repository.outlets import OutletsRepository
from cfzcore.outlet import Outlet
from cfzcore.company import Company
from datetime import datetime
from ..presenters.outlets import (
    OutletShortFormatPresenter,
    VehiclepassPresenter)
from ..services.outlet import OutletService
from cfzpersistence.repository.outlets import OutletVehiclepassRepository


@view_config(route_name="company_outlets",
             permission="admin",
             renderer="json")
def list_company_outlets(request):
    from cfzpersistence.repository.company import CompanyRepository
    company_repository = CompanyRepository(DBSession)
    company_id = request.matchdict['id']
    company = company_repository.get(company_id)
    repository = OutletsRepository(DBSession)
    company = company_repository.get(company_id)
    outlets = repository.all_for_company(company)
    return [OutletShortFormatPresenter(outlet) for outlet in outlets]


@view_config(route_name="create_outlet",
             permission="admin",
             renderer="templates/kuta/outlet/create.jinja2")
def create_outlet_form(request):
    return dict(page="companies", company_id=request.matchdict['id'])


@view_config(
    route_name="outlet_show",
    renderer="templates/kuta/outlet/show.jinja2")
def outlet_show(request):
    outlet_repository = OutletsRepository(DBSession)
    outlet = outlet_repository.find_by_id(request.matchdict['id'])
    return dict(page='companies', outlet=outlet.name)


@view_config(route_name="outlet_api_create", renderer="json")
def outlet_api_create(request):
    from ..services.company_data_service import CompanyService
    if request.method == 'POST':
        outlet_service = OutletService()
        company = CompanyService().get_with_id(request.matchdict['id'])
        outlet = outlet_service.create_outlet(company, **request.json_body)
        return OutletShortFormatPresenter(outlet)


@view_config(
    route_name="outlet_api",
    request_method="GET",
    renderer="json")
def outlet_api(request):
    outlet_repository = OutletsRepository(DBSession)
    outlet = outlet_repository.find_by_id(request.matchdict['id'])
    return OutletShortFormatPresenter(outlet)


@view_config(
    route_name="outlet_api",
    request_method="PUT",
    renderer="json")
def update_status(request):
    outlet_repository = OutletsRepository(DBSession)
    outlet = outlet_repository.find_by_id(request.matchdict['id'])
    outlet.status = request.json_body['status']
    outlet = outlet_repository.persist(outlet)
    return OutletShortFormatPresenter(outlet)


@view_config(route_name="outlet_contact_numbers", renderer="json")
def contact_numbers(request):
    from ..presenters.outlets import ContactNumberPresenter
    outlet_repository = OutletsRepository(DBSession)
    outlet = outlet_repository.find_by_id(request.matchdict['id'])
    if request.method == 'GET':
        return ContactNumberPresenter(outlet)
    else:
        outlet.telephone_number = request.json_body.get(
            'telephone_number',
            None)
        outlet.fax_number = request.json_body.get('fax_number', None)
        outlet = outlet_repository.persist(outlet)
        return ContactNumberPresenter(outlet)


@view_config(route_name="outlet_address", renderer="json")
def outlet_address(request):
    from cfzcore.address import Address
    from ..presenters.outlets import AddressPresenter
    outlet_repository = OutletsRepository(DBSession)
    outlet = outlet_repository.find_by_id(request.matchdict['id'])
    if request.method == 'GET':
        return AddressPresenter(outlet)
    else:
        if outlet.address is not None:
            address = outlet.address
        else:
            address = Address()
        address.update(request.json_body.get('line1', None),
                       request.json_body.get('line2', None))
        outlet.address = address
        outlet_repository.persist(outlet)
        return AddressPresenter(outlet)


@view_config(route_name="outlet_manager", renderer="json")
def outlet_manager(request):
    from ..presenters.outlets import OutletManagerPresenter
    outlet_service = OutletService()
    outlet = outlet_service.get_outlet(request.matchdict['id'])
    if request.method == 'POST':
        manager = outlet_service.create_manager(**request.json_body)
        outlet.manager = manager
        outlet = outlet_service.persist(outlet)
    elif request.method == 'PUT':
        outlet = outlet_service.update(outlet, **request.json_body)
    else:
        pass
    return OutletManagerPresenter(outlet)


@view_config(route_name="outlet_stickers", renderer="json")
def outlet_stickers(request):
    outlet_repository = OutletsRepository(DBSession)
    outlet = outlet_repository.find_by_id(request.matchdict['id'])
    sticker_repository = OutletVehiclepassRepository(DBSession)
    stickers = sticker_repository.all_for_outlet(outlet)
    return [VehiclepassPresenter(_sticker) for _sticker in stickers]


@view_config(route_name="outlet_sticker", renderer="json")
def outlet_sticker(request):
    outlet_service = OutletService()
    outlet = outlet_service.get_outlet(request.matchdict['id'])
    if request.method == 'POST':
        sticker = outlet_service.add_vehiclepass(outlet, **request.json_body)
        return VehiclepassPresenter(sticker)
    if request.method == 'PUT':
        sticker = outlet_service.update_vehiclepass(
            outlet,
            **request.json_body)
        return VehiclepassPresenter(sticker)


@view_config(route_name="outlet_collection_sticker", renderer="json")
def outlet_collection_sticker(request):
    outlet_service = OutletService()
    outlet = outlet_service.get_outlet(request.matchdict['outlet_id'])
    if request.method == 'PUT':
        sticker = outlet_service.update_vehiclepass(
            outlet,
            **request.json_body)
        return VehiclepassPresenter(sticker)
