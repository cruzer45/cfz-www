from pyramid.view import view_config
from ..models import (user_repository,
                      group_repository)
from pyramid.httpexceptions import HTTPFound
from pyramid.url import route_url


@view_config(route_name="user_list",
             permission="admin",
             renderer="templates/kuta/admin/users/list.jinja2")
def list(request):
    users = user_repository.all()
    return dict(
        page="users",
        user_selection="list",
        users=users,
        total=len(users))


@view_config(route_name="user_new",
             permission="admin",
             renderer="templates/kuta/admin/users/form.jinja2")
def user_new(request):
    return dict(page="users", user_selection="create", action="/user/create")


@view_config(route_name="user_show",
             permission="admin",
             renderer="templates/kuta/admin/users/edit_form.jinja2")
def user_show(request):
    from ..presenters.user import UserPresenter
    user = user_repository.get(request.matchdict['id'])
    return dict(page="users",
                user_selection="list",
                user=UserPresenter(user),
                action="/user/update/"+str(user.id))


@view_config(route_name="user_update",
             permission="admin")
def user_update(request):
    user_view = route_url('user_show', request, id=request.matchdict['id'])
    list_view = route_url('user_list', request)
    user = user_repository.get(request.matchdict['id'])
    post_data = request.POST
    permissions = [group_repository.find(permission) for permission in request.params.getall('permissions')]
    permissions_to_delete = []
    for permission in user.groups:
        if permission not in permissions:
            permissions_to_delete.append(permission)
    post_data['permissions_to_delete'] = permissions_to_delete
    post_data['permissions'] = permissions
    if 'submit' in post_data:
        if request.session.peek_flash():
            return HTTPFound(location=user_view)
        else:
            _update_user(user, **post_data)
            request.session.flash(u'<div class="alert alert-info">User updated successfully!</div>')
    return HTTPFound(location=list_view)


@view_config(
    route_name="user_create",
    permission="admin")
def user_create(request):
    request.session.pop_flash()
    success_view = route_url('user_list', request)
    fail_view = route_url('user_new', request)
    post_data = request.POST
    post_data['permissions'] = request.params.getall('permissions')
    if 'submit' in post_data:
        _verify_required_fields(request, **post_data)
        if request.session.peek_flash():
            return HTTPFound(location=fail_view)
        else:
            _create_user(**post_data)
            request.session.flash(
                u'<div class="alert alert-info">User added successfully!</div>')
    return HTTPFound(location=success_view)


def _verify_required_fields(request, **kwargs):
    empty_username_message = u'<div class="alert alert-error">Name can not be blank.</div>'
    empty_email_message = u'<div class="alert alert-error">Email can not be blank.</div>'
    empty_password_message = u'<div class="alert alert-error">Password can not be blank.</div>'
    invalid_email = u'<div class="alert alert-error">Email ' + kwargs.get('email') + u' is already taken.</div>'
    username = kwargs.get('username', None)
    if not username or username is None: request.session.flash(empty_username_message)
    email = kwargs.get('email', None)
    if not email or email is None: request.session.flash(empty_email_message)
    password = kwargs.get('password', None)
    if not password or password is None: request.session.flash(empty_password_message)
    if kwargs.get('email') and user_repository.is_email_available(kwargs.get('email')) is False: request.session.flash(invalid_email)


def _create_user(**kwargs):
    from cfzcore.user_account import UserAccount
    user = UserAccount(kwargs.get('username'), kwargs.get('email'))
    user.set_password(kwargs.get('password'))
    permissions = (group_repository.find(permission) for permission in kwargs.get('permissions'))
    user = user_repository.add_permissions(user, permissions)


def _update_user(user, **kwargs):
    if kwargs.get('permissions_to_delete'):
        user_repository.remove_permissions(user, kwargs.get('permissions_to_delete'))
    user_repository.add_permissions(user, kwargs.get('permissions'))
    return user


