from pyramid.view import view_config

from ..models import (DBSession)
from cfzwww.presenters.vehicle_pass import (VehiclePassShortFormatPresenter,
                                            VehiclePassDetailedFormatPresenter)
from cfzwww.services.vehiclepass import CarStickersCrudService


@view_config(route_name="vehicle_pass_index",
             renderer='templates/vehicle_pass/index.jinja2')
def show(request):
    return dict(page='companies')


@view_config(route_name="vehicle_pass_list", renderer='json')
def vehicle_pass_list(request):
    vehicle_passes = all_vehiclepasses()
    passes = _getVehiclePassInShortFormat(vehicle_passes)
    return passes


def _getVehiclePassInShortFormat(vehicle_passes):
    return [VehiclePassShortFormatPresenter(vp) for vp in vehicle_passes]


@view_config(route_name='stickers', renderer='json')
def company_stickers(request):
    crud_service = CarStickersCrudService()
    stickers = crud_service.find_stickers(request.matchdict['id'])
    return [VehiclePassShortFormatPresenter(sticker) for sticker in stickers]


@view_config(
    route_name='sticker',
    request_method='PUT',
    renderer='json')
def update_sticker(request):
    crud_service = CarStickersCrudService()
    sticker = crud_service.update(**request.json_body)
    return VehiclePassDetailedFormatPresenter(sticker)


@view_config(
    route_name='sticker',
    renderer='json',
    request_method='POST')
def create_sticker(request):
    crud_service = CarStickersCrudService()
    user = _get_user(request)
    attributes = request.json_body
    attributes['issued_by'] = user.username
    sticker = crud_service.save(request.matchdict['id'],
                                **attributes)
    return VehiclePassDetailedFormatPresenter(sticker)


@view_config(route_name='max_stickers', renderer='json')
def max_stickers(request):
    from ..presenters.company import CompanySummaryPresenter
    from cfzpersistence.repository import CompanyRepository
    repository = CompanyRepository()
    company = repository.get(request.matchdict['id'])
    company.max_stickers = int(request.matchdict['max_stickers'])
    company = repository.persist(company)
    return CompanySummaryPresenter(company)


@view_config(route_name='vehicle_pass_create', renderer='json')
def vehicle_pass_create(request):
    crud_service = CarStickersCrudService()
    company_id = request.matchdict['id']
    vehicle_pass = crud_service.save(company_id, **request.json_body)
    return VehiclePassDetailedFormatPresenter(vehicle_pass)


def _get_user(request):
    from pyramid.security import authenticated_userid
    from ..models import user_repository
    user_id = authenticated_userid(request)
    return user_repository.get(user_id)


@view_config(route_name="vehicle_pass_search_by_owner", renderer="json")
def vehicle_pass_search_by_owner(request):
    owner_name = request.matchdict['owner_name']
    vehiclepass_results = search_by_owner(owner_name)
    vehicle_passes = _getVehiclePassInShortFormat(vehiclepass_results)
    return vehicle_passes


@view_config(route_name="max_passes", renderer="json")
def max_passes(request):
    from cfzpersistence.repository.company import CompanyRepository
    from ..presenters.company import CompanyDetailsPresenter
    repository = CompanyRepository(DBSession)
    company = repository.get(request.matchdict['id'])
    company.max_vehicles = int(request.matchdict['max_vehicles'])
    return CompanyDetailsPresenter(company)
